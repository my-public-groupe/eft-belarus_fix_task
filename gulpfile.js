var gulp = require('gulp');
var notify = require("gulp-notify");            // notify when complete
var rename = require("gulp-rename");            // rename the file
var rimraf = require('rimraf');                 // clear folder
var buffer = require('vinyl-buffer');
var gulpif = require('gulp-if');
var merge = require('merge-stream');
var gulpFilter = require('gulp-filter');
var prettify = require('gulp-html-prettify');   // pretify html code

// css tasks
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');
var spritesmith = require('gulp.spritesmith');
var sassbeautify = require('gulp-sassbeautify');
var gcmq = require('gulp-group-css-media-queries');
 

// var reload      = browserSync.reload;

var connectPHP = require('gulp-connect-php');

var browserSync = require('browser-sync');

gulp.task('browserSync', function() {
  browserSync({
    proxy:'127.0.0.1',
    port:8000
  });
});

gulp.task('php', function(){
  connectPHP.server({ base:'./public', keepalive:true, hostname: 'localhost', port:8000, open: false});
});


gulp.task('livereload', ['browserSync', 'php','sass:watch']);

// gulp.task('connect-sync', function() {
//   connect.server({}, function (){
//     browserSync.init({
//       proxy: '127.0.0.1:8000',
//       startPath: "/public/index.php"
//     });
//   });
 
//   gulp.watch('**/*.php').on('change', function () {
//     browserSync.reload();
//   });
// });


// img task
var imagemin = require('gulp-imagemin');        // minify size of img
var pngquant = require('imagemin-pngquant');

gulp.task('prettyhtml', function() {
  gulp.src(['resources/views/**/*.blade.php', '!resources/views/admin/**/*.blade.php'])
    .pipe(prettify({indent_char: ' ', indent_size: 2}))
    .pipe(gulp.dest('resources/views/'))
});

gulp.task('prettyscss', function () {
  gulp.src('resources/assets/sass/**/*.scss')
    .pipe(sassbeautify())
    .pipe(gulp.dest('resources/assets/sass/'))
})

gulp.task('imagemin', function(){
    return gulp.src('public/images/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('public/images/'));
})

gulp.task('sprite', function() {
    var spriteData = gulp.src('public/images/sprite/dist/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../images/sprite/sprite.png',
        cssName: '_sprite.scss',
        padding: 5,
        algorithm: 'top-down'
    }));

    var imgStream = spriteData.img
        // DEV: We must buffer our stream into a Buffer for `imagemin` 
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest('public/images/sprite/'));

    // Pipe CSS stream through CSS optimizer and onto disk 
    var cssStream = spriteData.css

        .pipe(gulp.dest('resources/assets/sass/mixins/'));

    // Return a merged stream to handle both `end` events 
    return merge(imgStream, cssStream);
});

gulp.task('sass', function() {
    return gulp.src('resources/assets/sass/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gcmq())
        .pipe(csscomb())
        .pipe(rename('main.css'))
        .pipe(gulp.dest('public/css/'))
        .pipe(minifyCSS(''))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('public/css/'))
});


gulp.task('sass:watch', function() {
    gulp.watch('resources/assets/sass/**/*.scss', ['sass']);
});