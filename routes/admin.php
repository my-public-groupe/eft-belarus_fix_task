<?php

/*
 *
 *  admin panel
 *
 */
Route::get('admin/login', [
    'uses'          => 'Admin\AdminController@getLogin',
    'middleware'    => ['web']
]);

Route::get('admin/logout', [
    'uses'          => 'Auth\AuthController@logout',
    'middleware'    => ['web']
]);

Route::post('admin/login',[
    'uses'          => 'Admin\AdminController@postLogin',
    'middleware'    => ['web']
]);

Route::group(['middleware' => ['web', 'admin']], function () {
    Route::get('admin', 'Admin\AdminController@index');
});

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('json/changevisibility' ,        'Admin\JsonController@getChangeVisibility');

    Route::resource('content',          'Admin\ContentController');

    Route::resource('tags',             'Admin\TagsController');

    Route::resource('lists',            'Admin\ListsController');

    Route::resource('categories',       'Admin\CategoriesController');
    
    Route::resource('accessories',      'Admin\AccessoriesController');

    Route::resource('products',         'Admin\ProductsController');

    Route::resource('users',            'Admin\UserController');

    Route::resource('parameters',       'Admin\ParametersController');

    Route::resource('models',           'Admin\ModelsController');

    Route::resource('manufacturers',    'Admin\ManufacturersController');

    Route::resource('news',             'Admin\NewsController');

    Route::get('json/remove-product-parameter', 'Admin\ParametersController@removeProductParameter');

    Route::get('json/get-category-parameters',  'Admin\ParametersController@getCategoryParameters');

    Route::get('json/get-photos',  'Admin\PhotosController@getJSONPhotos');

    Route::get('clear-cache',  'Admin\AdminController@clearCache');

});