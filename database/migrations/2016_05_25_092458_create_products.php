<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function($t){
			$t->increments('id');
			$t->string('name', 200);
			$t->string('name_ro', 200);
			$t->string('name_en', 200);
			$t->text('description');
			$t->text('description_ro');
			$t->text('description_en');
			$t->text('description_short');
			$t->text('description_short_ro');
			$t->text('description_short_en');
			$t->float('price', 15, 2);
			$t->float('sale_price', 15, 2);
			$t->boolean('enabled')->default(true);
			$t->boolean('top');
            $t->integer('views');
            $t->integer('quantity');
			$t->string('product_number', 50)->index();
			$t->float('radius', 8, 2)->index();
			$t->float('width', 8, 2)->index();
			$t->float('height', 8, 2)->index();
			$t->tinyInteger('vehicle_type')->index()->comment("0-легковой, 1-внедорожник");
			$t->integer('model_id')->index();
			$t->string('img_100x200', 50);
			$t->string('imgs', 50);
			$t->string('reserve', 10);
			$t->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
