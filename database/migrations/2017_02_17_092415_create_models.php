<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function($t){
            $t->increments('id');
            $t->string('name', 200);
            $t->string('name_ro', 200);
            $t->string('name_en', 200);
            $t->text('description');
            $t->text('description_ro');
            $t->text('description_en');
            $t->text('description_short');
            $t->text('description_short_ro');
            $t->text('description_short_en');
            $t->integer('price');
            $t->boolean('enabled')->default(true);
            $t->string('slug', 200)->index();
            $t->integer('manufacturer_id')->unsigned();
            $t->integer('category_id')->unsigned();
            $t->integer('sort');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('models');
    }
}
