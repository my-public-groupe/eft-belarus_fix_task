$("#main-slider").owlCarousel({items: 1, dots: false, nav: false, mouseDrag: false});
$(document).on("click", ".productpackage", function () {
    var product_photo = $(this).data("photo");
    var product_price = $(this).data("price");
    var product_description = $(this).data("description");
    var product_modal = $("#product-modal");
    product_modal.find("#product-image").attr("src", "uploaded/" + product_photo);
    product_modal.find("#product-price").text(product_price);
    product_modal.find("#product-description").empty().append(product_description);
    product_modal.modal();
});
$(function () {
    var location_url = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
    $("ul.nav.sf-menu li a").each(function (index, value) {
        var href = $(value).attr("href");
        var menu_url = href.substr(href.lastIndexOf("/") + 1);
        if (location_url === menu_url) {
            $("ul.nav.sf-menu").find("li").find("a").removeClass("active");
            $(this).addClass("active");
        }
    });

    $(document).on('click', '.js-mobile', function(event) {
        event.preventDefault();
        $(this).next().toggleClass('hidden');
        $(this).children('.slicknav_icon').toggleClass('active');
    });
});
$("#btn_frm_model").click(function () {
    $(this).closest("form").submit();
});
$('.google-map').click(function () {
    $(this).find('iframe').addClass('clicked')
}).mouseleave(function () {
    $(this).find('iframe').removeClass('clicked')
});

$(function () {
    $("#subscribe-frm").validate({
        rules:{
            email: {
                required: true,
                email: true
            }
        },
        errorPlacement: function(error,element) {
            return true;
        },
        submitHandler: function (form) {
            $.ajax({
                method: "POST",
                url: $(form).attr("action"),
                data: $(form).serialize(),
                success: function () {
                    swal({
                        type: 'success',
                        title: 'Спасибо! Вы успешно подписались на рассылку!',
                        showConfirmButton: false,
                        timer: 5000
                    });

                    $(form).trigger('reset');
                }
            });

            return false;
        }
    });

    $("#contact-form").validate({
        rules:{
            fio: {required: true},
            email: {required: true, email: true},
            msg: {required: true}
        },
        errorPlacement: function(error,element) {
            return true;
        },
        submitHandler: function (form) {
            $.ajax({
                method: "POST",
                url: $(form).attr("action"),
                data: $(form).serialize(),
                success: function () {
                    swal({
                        type: 'success',
                        title: 'Спасибо! Мы получили Ваше сообщение',
                        showConfirmButton: false,
                        timer: 5000
                    });

                    $(form).trigger('reset');
                }
            });

            return false;
        }
    });
});