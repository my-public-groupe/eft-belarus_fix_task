var moscow =
{
    'address': 'Россия, 127015, г. Москва, ул. Новодмитровская д.2, к. 2, 9 этаж, БЦ «Савеловский сити»',
    'phone': '+7 (495) 212-1717',
    'email': '<br><a href="mailto:info@eftgroup.ru">info@eftgroup.ru</a> - Отдел продаж современных измерительных технологий<br><a href="mailto:support@eftgroup.ru">support@eftgroup.ru</a> - Техническая поддержка<br><a href="mailto:service@eftgroup.ru">service@eftgroup.ru</a> - Сервисный центр'
};
var vologda =
{
    'address': 'Россия, 160001, г.Вологда, пр-кт Победы, д.33, оф. 47',
    'phone': '+7 (8172) 708-908',
    'email': '<br><a href="mailto: vologda@eftgroup.ru">vologda@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
/*var voronezh =
{
    'address': 'Россия, 394026, г. Воронеж, проспект Труда, д. 48, оф. 97',
    'phone': '+7 (473) 233-3283',
    'email': '<br><a href="mailto:voronezh@eftgroup.ru">voronezh@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};*/
var irkutsk =
{
    'address': 'Россия, 664047, г. Иркутск, ул. Трудовая, д. 60, оф. 10в',
    'phone': '+7 (3952) 532-515',
    'email': '<br><a href="mailto:irk@eftgroup.ru">irk@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
var kaliningrad =
{
    'address': 'Россия, 236005, г. Калининград, ул. П.Морозова, д. 96, оф. 9',
    'phone': '+7 (952) 798-2337',
    'email': '<br><a href="mailto:kln@eftgroup.ru">kln@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
var krasnodar =
    {
        'address': 'Россия, 350059, г. Краснодар, ул. Уральская, д. 75/1, оф. 707 (7 эт.), БЦ "AVM"',
        'phone': '+7 (861) 201-8345',
        'email': '<br><a href="mailto:krd@eftgroup.ru">krd@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
    };
/*var krasnoyarsk =
{
    'address': 'Россия, 660018, г. Красноярск, ул. Толстого, д. 17 оф. 189',
    'phone': '+7 (391) 278-3326; +7 (391) 298-6109',
    'email': '<br><a href="mailto:kln@eftgroup.ru">aak@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};*/
var orenburg =
{
    'address': 'Россия, 460026, г. Оренбург, ул. Проспект Победы, д. 114, оф. 304',
    'phone': '+7 (353) 240-0033',
    'email': '<br><a href="mailto:oren@eftgroup.ru">oren@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
var stpetersburg =
{
    'address': 'Россия, 196247, г. Санкт-Петербург, Ленинский проспект, д. 153, оф. 345',
    'phone': '+7 (812) 777-5115',
    'email': '<br><a href="mailto:spb@eftgroup.ru">spb@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
var saratov =
{
    'address': 'Россия, 410012, г. Саратов, ул. Рабочая, д. 145А, оф. 501. 5 этаж, БЦ «Самсон»',
    'phone': '+7 (8452) 398-182',
    'email': '<br><a href="mailto:saratov@eftgroup.ru">saratov@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
var stavropol =
{
    'address': 'Россия, 355042, г. Ставрополь, 1-й параллельный проезд, д. 8, оф. 208',
    'phone': '+7 (8652) 330-028',
    'email': '<br><a href="mailto:stav@eftgroup.ru">stav@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};
/*var tambov =
{
    'address': 'Россия, 392000 г. Тамбов, ул. Мичуринская 7, пом. 2а',
    'phone': '+7 (4752) 723-773',
    'email': '<br><a href="mailto:tambov@eftgroup.ru">tambov@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};*/
var habarovsk =
{
    'address': 'Россия, 680022, г. Хабаровск, ул. Воронежская, д. 47-А, оф. 1002 (10 эт.), БЦ \"ОПОРА\"',
    'phone': '+7 (4212) 788-298',
    'email': '<br><a href="mailto:khab@eftgroup.ru">khab@eftgroup.ru</a> - Отдел продаж современных измерительных технологий'
};