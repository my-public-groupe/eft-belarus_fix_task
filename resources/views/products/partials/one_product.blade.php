<div class="col-md-4 col-sm-4" layout="column" layout-align="start stretch">
  <div md-whiteframe-1dp="" class="product-container">
    @if ($product->top == 1)
    <div class="content-shop-status-red">
      <span>Топ</span>
    </div>
    @endif @if ($product->sale_price != 0)
    <div class="content-shop-status-blue">
      <span>%</span>
    </div>
    @endif @if (in_array($product->getSeason(), [1, 2])) {{-- всесезонные или зимние--}}
    <div class="content-shop-season icon-season-winter"></div>
    @endif @if(in_array($product->getSeason(), [1, 3])) {{-- всесезонные или летние--}}
    <div class="content-shop-season icon-season-summer"></div>
    @endif
    <div class="item-image">
      <a href="{{ route('get-product', [$product->model->manufacturer->slug, $product->model->slug, $product->id]) }}"><img src="{{ $product->getSmallPhotoPath() }}"></a>
    </div>
    <a href="{{ route('get-product', [$product->model->manufacturer->slug, $product->model->slug, $product->id]) }}" class="item-title">{{ $product->getFullName() }}</a>
    <div class="item-qty" layou="row" layout-align="space-between center">
      @if($product->product_number != '')<span class="item-article">арт. {{ $product->product_number }}</span>@endif
      <div class="item-qty-input" layout="row" layout-align="end start">
        <md-button class="button" ng-click="q=(q-1) || 1">-</i>
        </md-button>
        <input type="text" ng-model="q" ng-init="1" value="1" value="4" disabled="disabled">
        <md-button class="button" ng-click="q = q + 1">+</md-button>
      </div>
    </div>
    <div class="item-cart" layout="row" layout-align="space-between center">
      <div class="item-cart-price" layout="column" layout-align="space-around start">
        @if($product->price != 0)
        <div class="price-new"><span>{{ $product->price }}</span> руб.</div>@endif @if($product->sale_price != 0)
        <div class="price-old"><span>{{ $product->sale_price }}</span> руб.</div>@endif
      </div>
      <md-button ng-show="!inCart()" class="item-cart-buy button button-small button-blue" ng-click="ngCart.addItem({{ $product->id }}, {{ $product->getFullName() }}, {{ $product->price }}, 4, data);">
        В корзину
      </md-button>
      <md-button ng-show="inCart()" class="item-cart-buy button button-small button-red" ng-click="ngCart.removeItemById({{ $product->id }})">
        Удалить
      </md-button>
    </div>
  </div>
</div>
