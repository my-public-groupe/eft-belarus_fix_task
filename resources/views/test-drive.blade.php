@extends('body')
@section('title', 'Test-drive')
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">Test-drive</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header no-margin text-center">Test-drive</h2>
            <br>
            <img src="images/testdrive.png" class="img-responsive center-block">
            <br>
        </div>
    </div>
    <div class="test-drive-container">
        @if(!empty($testdrive_page))
            {!! $testdrive_page->description !!}
        @endif

        <form role="form" method="post" action="{{ route('send-test-drive') }}" class="test-drive-frm-page">
            <div class="form-group">
                <label for="product">Оборудование*</label>
                <select class="form-control" name="product" id="product">
                    <option value="">--- Выберите оборудование ---</option>
                    <option value="">Приемник EFT M1 Plus</option>
                        <option value="">Приемник EFT M2 GNSS</option>
                        <option value="">Приемник EFT M3 GNSS</option>
                        <option value="">Приемник EFT M4 GNSS</option>
                        <option value="">Приемник EFT S1 GNSS</option>
                </select>
            </div>
            <div class="form-group">
                <label for="testdrive_fio">ФИО*</label>
                <input type="text" class="form-control" name="fio" id="fio" placeholder="ФИО">
            </div>
            <div class="form-group">
                <label for="testdrive_company">Название компании*</label>
                <input type="text" class="form-control" name="company" id="company" placeholder="Название компании">
            </div>
            <div class="form-group">
                <label for="testdrive_phone">Телефон*</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Телефон">
            </div>
            <div class="form-group">
                <label for="testdrive_message">Комментарий</label>
                <textarea class="form-control" name="comment" id="comment" placeholder="Комментарий"></textarea>
            </div>
            <input type="hidden" name="g-recaptcha-response" class="g-recaptcha">
            <button type="submit" class="btn btn-theme btn-testdrive"><span class="glyphicon glyphicon-fire"></span> Заказать тест-драйв!</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $(".test-drive-frm-page").validate({
                rules:{
                  //  product: {required: true},
                    fio: {required: true},
                    company: {required: true},
                    phone: {required: true},
                    email: {
                        required: true,
                        email: true
                    }
                },
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function (form) {
                    $.ajax({
                        method: "POST",
                        headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                        url: $(form).attr("action"),
                        data: $(form).serialize(),
                        success: function () {
                            swal({
                                type: 'success',
                                title: 'Спасибо! Ваша заявка отправлена',
                                showConfirmButton: false,
                                timer: 5000
                            });

                            $(form).trigger('reset');
                        }
                    });

                    return false;
                }
            });
        });
    </script>
@endsection