@extends('body')
@section('title', 'EFT-PLUS - Контакты')
@section('centerbox')
    <!-- BREADCRUMBS -->
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">Контакты</li>
    </ol>
    <script type="application/ld+json">
            {
             "@context": "http://schema.org",
             "@type": "BreadcrumbList",
             "itemListElement":
             [
              {
               "@type": "ListItem",
               "position": 1,
               "item":
               {
                "@id": "{{ route('index') }}",
                "name": "Главная"
                }
              },
              {
               "@type": "ListItem",
               "position": 2,
               "item":
               {
                "@id": "{{ route('contacts') }}",
                "name": "Контакты"
                }
              }
             ]
            }


    </script>

    <!-- /BREADCRUMBS -->

    <!-- PAGE -->
    <section class="page-section color contacts">
        <div class="row">
            <div class="col-sm-12">
                <div class="contact-info">
                    @if(!empty($contacts_list))
                        <h2>{{$contacts_list->name}}</h2>
                        <div class="media-list">
                            <div class="media">
                                <i class="pull-left fa fa-home"></i>
                                <div class="media-body">
                                    <strong>Адрес:</strong><br>
                                    <span id="city-address">{{$contacts_list->description_short}}</span>
                                </div>
                            </div>
                            <div class="media">
                                <i class="pull-left fa fa-phone"></i>
                                <div class="media-body">
                                    <strong>Телефон/Факс:</strong><br>
                                    <span id="city-phone">{{$contacts_list->description_short_ro}}</span>
                                </div>
                            </div>
                            <span id="city-email"><br>{!! $contacts_list->description !!}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- /PAGE -->

    <!-- PAGE -->
    <div class="row">
        <h2>Схема проезда</h2>
        <!-- Yandex map -->
        @if(isset($contacts_list->description_short_en))
            <div>
                <iframe src="{{$contacts_list->description_short_en}}" width="100%" height="400"
                        frameborder="0"></iframe>
            </div>
        @endif
    </div>
    <!-- /PAGE -->
@endsection