<div class="remodal" data-remodal-options='{ "hashTracking": false }' data-remodal-id="loginForm" role="dialog" aria-labelledby="Login Pop Up" aria-describedby="loginForm" id="loginForm">
  <md-button class="modal-close" data-remodal-action="close" aria-label="modal-close">
    <md-icon md-svg-src='images/icons/svg/close.svg'></md-icon>
  </md-button>
  <h2 class="modal-title">Вход</h2>
  <form id="authForm" role="form" method="post" layout-align="start start" layout="column" name="authForm" action="">
    <div class="form-group">
      <md-input-container class="md-block">
        <label>Ваш Email</label>
        <input class="form-control" name="email" ng-model="auth.authEmail" required minlength="4" ng-pattern="/^.+@.+\..+$/" type="email">
        <div ng-messages="authForm.email.$error" role="alert">
          <div ng-message="required">Это поле обязательное.</div>
          <div ng-message="minlength">Ваш email должен содержать не менее 4 символов и должен быть достоверным</div>
          <div ng-message="pattern">Email должен быть достоверным.</div>
        </div>
      </md-input-container>
    </div>
    <div class="form-group">
      <md-input-container class="md-block">
        <label>Ваш пароль</label>
        <input class="form-control" ng-minlength="2" required name="password" ng-model="auth

                            .password" type="password">
        <div ng-messages="authForm.password.$error" role="alert" multiple>
          <div ng-message="required">Это поле обязательное.</div>
          <div ng-message="minlength">Пароль должен содержать не меньше 2 символов.</div>
        </div>
      </md-input-container>
    </div>
    <div layout="row" class="modal-buttons">
      <md-button type="submit" class="md-raised md-primary modal-enter">Войти</md-button>
      <md-button class="md-link modal-password">Забыли пароль</md-button>
    </div>
    <hr>
    <md-button class="md-raised modal-register" data-remodal-target="regForm">Регистрация</md-button>

  </form>
</div>

<div class="remodal" data-remodal-options='{ "hashTracking": false }' data-remodal-id="regForm" role="dialog" aria-labelledby="Register Pop Up" aria-describedby="regForm" id="regForm">
  <md-button class="modal-close" data-remodal-action="close" aria-label="modal-close">
    <md-icon md-svg-src='images/icons/svg/close.svg'></md-icon>
  </md-button>
  <h2 class="modal-title">Регистрация</h2>
  <form id="registerForm" role="form" method="post" layout-align="start start" layout="column" name="regForm" action="">
    <div class="form-group">
      <md-input-container class="md-block">
        <label>Ваш Email</label>
        <input class="form-control" name="email" ng-model="reg.authEmail" required minlength="4" ng-pattern="/^.+@.+\..+$/" type="email">
        <div ng-messages="regForm.email.$error" role="alert">
          <div ng-message="required">Это поле обязательное.</div>
          <div ng-message="minlength">Ваш email должен содержать не менее 4 символов и должен быть достоверным</div>
          <div ng-message="pattern">Email должен быть достоверным.</div>
        </div>
      </md-input-container>
    </div>

    <div class="passwordGroup" layout="row" layout-wrap layout-align-gt-xs="space-between start" layout-align-xs="space-between stretch">
      <div class="form-group" flex="45">
        <md-input-container class="md-block">
          <label>Ваш пароль</label>
          <input type="password" class="form-control" ng-minlength="2" required name="regPassword" ng-model="reg.password">
          <div ng-messages="regForm.regPassword.$error" role="alert" multiple>
            <div ng-message="required">Это поле обязательное.</div>
            <div ng-message="minlength">Пароль должен содержать не меньше 2 символов.</div>
          </div>
        </md-input-container>
      </div>
      <div class="form-group" flex="45">
        <md-input-container class="md-block">
          <label>Повторите пароль</label>
          <input type="password" class="form-control" ng-minlength="2" required name="regConfirmPassword" ng-model="reg.passwordRepeat">
          <div ng-messages="registerForm.regPassword.$error" role="alert" multiple>
            <div ng-message="required">Это поле обязательное.</div>
            <div ng-message="minlength">Пароль должен содержать не меньше 2 символов.</div>
          </div>
        </md-input-container>
      </div>
    </div>
    <div class="form-group">
      <md-input-container class="md-block">
        <label>Ваше имя</label>
        <input class="form-control" ng-minlength="2" required name="regName" ng-model="reg.name">
        <div ng-messages="regForm.regName.$error">
          <div ng-message="required">Это поле обязательное.</div>
          <div ng-message="minlength">Имя должно содержать не меньше 2 символов.</div>
        </div>
      </md-input-container>
    </div>
    <div class="form-group">
      <md-input-container class="md-block">
        <label>Телефон</label>
        <input class="form-control" ng-pattern="/^[0-9 ]+$/" ng-minlength="4" type="text" required name="phone" ng-model="event.phone">
        <div ng-messages="regForm.phone.$error" role="alert" multiple>
          <div ng-message="required">Это поле обязательное.</div>
          <div ng-message="minlength">Телефон долен содержать не меньше 4 символов.</div>
          <div ng-message="pattern">Телефон должно содержать только цифры.</div>
        </div>
      </md-input-container>
    </div>
    <div layout="row" class="modal-buttons">
      <md-button type="submit" class="md-raised md-primary modal-register">Зарегистрироваться</md-button>
    </div>

  </form>
</div>