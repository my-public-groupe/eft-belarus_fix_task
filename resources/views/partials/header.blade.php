<header class="header @if(Route::currentRouteName() != "index")header-custom @endif">
    <div class="header-wrapper">
        <div class="container top-menu">
            <!-- Logo -->
            <div class="col-sm-12 col-md-2 col-lg-2">
                <div class="logo">
                    <a href="{{ route('index') }}"><img width="232" src="images/eftplus2.png" alt="EFT-PLUS"/></a>
                </div>
                <div class="mobile-logo">
                    <a href="http://eftgroup.ru"><img width="160" src="images/logo-grey.png" alt="EFT-GROUP"/></a>
                </div>
            </div>
            <!-- /Logo -->

            <div class="col-lg-2 col-md-2 col-sm-12 phone-holder">
                    <div class="phone-container">
                        <span class="i-phone"></span>
                        <span class="phone-cont">
                            <a href="tel:+375291897991">+375 (29) 189 79 91</a>
                        </span>
                    </div>
                    <div class="email-container">
                        <span class="i-mail"></span>
                        <a href="mailto:info@eft.by">info@eft.by</a>
                    </div>
            </div>

            <!-- Navigation -->
            <div class="col-sm-12 col-md-7 col-lg-7 menu-wrap hidden-xs">
                <nav class="navigation closed clearfix">
                    <!-- navigation menu -->
                    <ul class="nav sf-menu">
                        <li><a class="active" href="{{route('index')}}" id="main">Главная</a></li>
                        <li><a href="{{ route('content', 'about') }}">О компании</a></li>
                        <li><a href="{{ route('news') }}">Новости</a></li>
                        <li><a href="{{ route('contacts') }}">Контакты</a></li>
                    </ul>
                    <!-- /navigation menu -->
                </nav>
            </div>
            <div class="mobile-menu">
                <a href="javascript:void(0);" class="js-mobile slicknav_btn slicknav_collapsed clearfix">
                    <span class="slicknav_menutxt">Меню</span>
                    <span class="slicknav_icon">
							<span class="slicknav_icon-bar"></span>
							<span class="slicknav_icon-bar"></span>
							<span class="slicknav_icon-bar"></span>
						</span>
                </a>
                <div class="links hidden">
                    <a class="active" href="{{route('index')}}" id="main">Главная</a>
                    <a href="{{ route('content', 'about') }}">О компании</a>
                    <a href="{{ route('news') }}">Новости</a>
                    <a href="{{ route('contacts') }}">Контакты</a>
                </div>
            </div>
            <div class="menu-category js-cat" itemscope="" itemtype="http://schema.org/ItemList">
                <h3 itemprop="name">Категории</h3>
                <meta itemprop="itemListOrder" content="http://schema.org/ItemListOrderDescending">
                <div class="mobile-cat">
                    <ul>
                        @if($categories->count() > 0)
                            @foreach($categories as $category)
                                <li><a href="{{ route('get-category', $category->slug) }}" itemprop="url">{{$category->name}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <!-- /Navigation -->
            <div class="col-lg-1 col-md-1">
                <div class="eftgroup-logo">
                    <a href="http://eftgroup.ru"><img src="images/logo-grey.png" alt="EFT-GROUP"/></a>
                </div>
            </div>
        </div>
    </div>
</header>