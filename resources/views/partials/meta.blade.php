@if ($data->getTitle() != "") 
    @section('title', $data->getTitle())
@else
    @section('title', (isset($title) ? $title : ''))
@endif
@section('meta_keywords', $data->getMetaKeywords())
@section('meta_description', $data->getMetaDescription())