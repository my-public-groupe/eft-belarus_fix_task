@extends('body')
@section('title', $category->name)
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{ route('index') }}">Главная</a></li>
        <li class="active">{{$category->name}}</li>
    </ol>
    <div class="product-category">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-header no-margin">{{$category->name}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 no-margin">
                @if($category->bannerImage !== null)
                    <div class="category-description">
                        {!! $category->bannerImage !!}
                    </div>
                @endif
                    @if($models->count() > 0)
                        <div class="col-sm-12 margin-bottom">
                            @foreach ($models as $product)
                                <div class="col-sm-4">
                                    <div class="thumbnail no-border no-padding thumbnail-car-card">
                                        <div class="media">
                                            <a class="media-link"
                                            href="{{route('get-model', [$category->slug, $product->slug])}}">
                                                @if(isset($product->photos{0}))
                                                    <img src="uploaded/{{$product->photos{0}->source}}"
                                                        alt="{{$product->name or ''}}<"/>
                                                @endif
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <span class="caption-title"><a
                                                        href="{{route('get-model', [$category->slug, $product->slug])}}">{{$product->name or ''}}</a></span>
                                            <div class="caption-text">{{$product->price}} руб.</div>
                                            <div class="buttons">
                                                <a class="btn btn-theme ripple-effect"
                                                href="{{route('get-model', [$category->slug, $product->slug])}}">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                <div class="category-description">
                    {!! $category->description !!}
                </div>
            </div>
        </div>
    </div>
@endsection