@extends('body')
@section('title', 'Парк оборудования')
@section('centerbox')
    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs text-right">
        <div class="container">
            <div class="page-header">
                <h1>@yield('title')</h1>
            </div>
            <ul class="breadcrumb">
                <li><a href="{{ route('index') }}">Главная</a></li>
                <li class="active">@yield('title')</li>
            </ul>
        </div>
    </section>
    <!-- /BREADCRUMBS -->

    <!-- PAGE WITH SIDEBAR -->
    @foreach($categories as $category)
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="section-title">{{$category->name or ''}}</div>
                <!-- CONTENT -->
                <div class="swiper swiper--category-{{$category->id}}">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                                @foreach($category->models as $model)
                                <div class="swiper-slide">
                                    <div class="thumbnail no-border no-padding thumbnail-car-card">
                                        <div class="media">
                                            <a class="media-link" href={{ route('get-model', [$category->slug, $model->slug]) }}>
                                                @if(isset($model->photos{0}))
                                                <img src="uploaded/{{$model->photos{0}->source}}" alt="{{$model->name or ''}}<"/>
                                                @endif
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <span class="caption-title"><a href={{ route('get-model', [$category->slug, $model->slug]) }}>{{$model->name or ''}}</a></span>
                                            <div class="caption-text">{{$model->price}} руб.</div>
                                            <div class="buttons">
                                                <a class="btn btn-theme ripple-effect" href="{{ route('get-model', [$category->slug, $model->slug]) }}">Подробнее</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
                    <div class="swiper-button-next category-{{$category->id}}"><i class="fa fa-angle-right"></i></div>
                    <div class="swiper-button-prev category-{{$category->id}}"><i class="fa fa-angle-left"></i></div>
                </div>
                <!-- /CONTENT -->
            </div>
        </section>
        {{--<div class="text-center">
            <a class="btn btn-theme ripple-effect" href="#">Все {{$category->name}}</a>
        </div>--}}
    @endforeach

    {!! HTML::script('js/plugins/jquery/jquery-1.11.1.min.js') !!}

    {{--Начинается муть--}}
    <script type="text/javascript">
        @foreach($categories as $category)
        var swiperCat_{{$category->id}} = $('.swiper--category-{{$category->id}} .swiper-container');

        $(function () {
            if ($().swiper) {
                if (swiperCat_{{$category->id}}.length) {
                    swiperCat_{{$category->id}} = new Swiper(swiperCat_{{$category->id}}, {
                        direction: 'horizontal',
                        slidesPerView: 4,
                        spaceBetween: 30,
                        //autoplay: 2000,
                        loop: false,
                        paginationClickable: true,
                        pagination: '.swiper-pagination',
                        nextButton: '.swiper-button-next.category-{{$category->id}}',
                        prevButton: '.swiper-button-prev.category-{{$category->id}}'
                    });
                }
            }
        });
        @endforeach

        function updater() {
            @foreach($categories as $category)
            if ($().swiper) {
                if (typeof (swiperCat_{{$category->id}}.length) == 'undefined') {
                    swiperCat_{{$category->id}}.update();
                    swiperCat_{{$category->id}}.onResize();
                    if ($(window).width() > 991) {
                        swiperCat_{{$category->id}}.params.slidesPerView = 4;
                        //swiperOffersBest.stopAutoplay();
                        //swiperOffersBest.startAutoplay();
                    }
                    else {
                        if ($(window).width() < 768) {
                            swiperCat_{{$category->id}}.params.slidesPerView = 1;
                            //swiperOffersBest.stopAutoplay();
                            //swiperOffersBest.startAutoplay();
                        }
                        else {
                            swiperCat_{{$category->id}}.params.slidesPerView = 2;
                            //swiperOffersBest.stopAutoplay();
                            //swiperOffersBest.startAutoplay();
                        }
                    }
                }
            }
            @endforeach
        }

        $(window).resize(function () {
            // Refresh isotope
            if ($().isotope) {
                isotopeContainer.isotope('reLayout'); // layout/relayout on window resize
            }
            updater();
        });

        //Подсчет margin-bottom для блоков
        $(function () {
            $(".caption-title").find("a").each(function (index) {
               if($(this).text().length < 23){
                   $(this).parent().css("display", "block");
                   $(this).parent().css("margin-bottom", "25px");
               }
            });
        });

    </script>
@endsection