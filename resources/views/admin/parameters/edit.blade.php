@extends('admin.body')
@section('title', 'Параметры')


@section('centerbox')
<div class="page-header">
    <h1> <a href="{{ URL::to('admin/parameters') }}">Параметры</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
</div>

@include('admin.partials.errors')

@if(!isset($data))
{{ Form::open(['url' => 'admin/parameters', 'class' => 'form-horizontal']) }}
@else
{{ Form::open(['url' => 'admin/parameters/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
@endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group lang-ru">
                {{ Form::label('name[ru]', 'Название', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro">
                {{ Form::label('name[ro]', 'Название рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Название англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('type', 'Тип', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('type', config('custom.parameter_types'), ((isset($data->type)) ? $data->type : 0), ['class' => 'form-control col-sm-11 col-xs-12']) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->

    <div class="space"></div>

    <div class="tabbable hide">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#values" data-toggle="tab">Значения</a>
            </li>
        </ul>

        <div class="tab-content">
             <div class="tab-pane active" id="values">
                 <div class="tab-content">
                     <ul id="sortable">
                         @if (isset($data))
                             @foreach($data->values as $item)
                                 @include('admin.parameters.one_value')
                             @endforeach
                         @else
                             @include('admin.parameters.one_value')
                         @endif
                     </ul>
                     <a href="javascript:addItem();" class="btn btn-sm btn-info">
                         <i class="ace-icon fa fa-plus-circle bigger-110"></i>
                         Добавить
                     </a>
                 </div>
             </div>
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>
{{ Form::close() }}
@endsection

@section('scripts')

    {!! HTML::script('ace/assets/js/jquery-ui.js') !!}

    <script>
        $(document).ready(function() {

            $('#sortable').sortable();

            if ($('select[name=type]').val() == 1) {
                $('.tabbable').removeClass('hide');
            }

            initDelete();
        });

        $('select[name=type]').on('change', function() {
            if ( $('select[name=type]').val() == 1 ){
                $('.tabbable').removeClass('hide');
            }else{
                $('.tabbable').addClass('hide');
            }
        });

        function addItem() {
            $clone = $('#sortable li:last-child').clone();
            $clone.find('input').val("");
            $('#sortable').append($clone);

            initDelete();
        }

        function initDelete() {
            $('#sortable li i.delete').click(function() {
                if ($(this).closest('li').find('input[type=hidden]').val() == 0) {
                    $(this).closest('li').remove();
                } else {
                    toastr.error("Требует доработки в базе. Нужна проверка если нигде не задействован данный параметр");
                }
            });
        }
    </script>
@endsection


@section('styles')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />

    <style>
        #sortable {
            border: 1px solid #eee;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 5px 0 0 0;
            margin-right: 10px;
            width: 100%;
        }

        #sortable li {
            margin: 0 5px 5px 5px;
            padding: 5px;
            border: 1px solid #ccc;
            background-color: #cce2c1;
        }
    </style>
@endsection