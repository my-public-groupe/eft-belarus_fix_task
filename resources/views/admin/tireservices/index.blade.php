@extends('admin.common.list',
    [
        'title'       =>  'Шиномонтажи',
        'desc'        =>  'Список сервисов шиномонтажа',
        'model'       =>  'tire-services',
        'fields'      =>  ['name' => 'Наименование', 'created_at' => 'Создан'],
        'data'        =>  $data,
        'no_visibility' => true,
    ]
)



