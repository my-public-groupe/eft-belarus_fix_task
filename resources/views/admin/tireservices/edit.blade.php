@extends('admin.body')
@section('title', 'Контент')


@section('centerbox')
<div class="page-header">
    <h1> <a href="{{ URL::to('admin/tire-services') }}">Шиномонтажи</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование </small> </h1>
</div>

@include('admin.partials.errors')

@if(!isset($data))
{{ Form::open(['url' => 'admin/tire-services', 'class' => 'form-horizontal']) }}
@else
{{ Form::open(['url' => 'admin/tire-services/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
@endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <!--
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-yellow btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить и закрыть</button>
            </div>-->
            <div class="col-sm-2 ">
                <label>
                    {{ Form::checkbox('enabled',  1, (isset($data) && $data->enabled == 1 ? true : false), ['class' => 'ace']) }}
                    <span class="lbl hide"> Включить </span>
                </label>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        
                        @if (isset($data))
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa- bigger-120 green"></i>
                            ID: {{ $data->id }}
                        </div>
                        
                        <div class="btn btn-link">
                            <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                             Изменен: {{ $data->updated_at }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('name[ro]', 'Заголовок рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Заголовок англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at) ? date('d-m-Y', strtotime($data->created_at)) : old('date', Date::now()->format('d-m-Y'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                 {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <div class="space"></div>
    <div class="row">
        <div class="col-sm-6">
        	<div class="form-group">
                {{ Form::label('description_short[ru]', 'Адрес', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('description_short[ru]', (isset($data->description_short) ? $data->description_short : old('description_short')), array('class' => 'col-sm-11 col-xs-12 description_short')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('description_short[ro]', 'Адрес рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('description_short[ro]', (isset($data->description_short_ro) ? $data->description_short_ro : old('description_short_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('description_short[en]', 'Адрес англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('description_short[en]', (isset($data->description_short_en) ? $data->description_short_en : old('description_short_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        
        </div>
        <div class="col-sm-6">
        	<div class="form-group">
                {{ Form::label('phone[ru]', 'Телефон', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('phone[ru]', (isset($data->phone) ? $data->phone : old('phone')), array('class' => 'col-sm-11 col-xs-12 phone_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('phone[ro]', 'Телефон рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('phone[ro]', (isset($data->phone_ro) ? $data->phone_ro : old('phone_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('phone[en]', 'Телефон англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('phone[en]', (isset($data->phone_en) ? $data->phone_en : old('phone_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
			<div class="form-group">
                {{ Form::label('working_hours[ru]', 'Часы работ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('working_hours[ru]', (isset($data->working_hours) ? $data->working_hours : old('working_hours')), array('class' => 'col-sm-11 col-xs-12 working_hours_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro" >
                {{ Form::label('working_hours[ro]', 'Часы работ рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('working_hours[ro]', (isset($data->working_hours_ro) ? $data->working_hours_ro : old('working_hours_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('working_hours[en]', 'Часы работ англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('working_hours[en]', (isset($data->working_hours_en) ? $data->working_hours_en : old('working_hours_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('map', 'Координаты:', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                    <div class="col-sm-9 showTip L5" >
                        {{ Form::text('map[X]', ((isset($data->map) && $data->map != '') ? $data->map->X : old('map[X]')), array('class' => 'col-sm-5', 'placeholder' => 'X', 'id' => 'mapX')) }}
                        {{ Form::text('map[Y]', ((isset($data->map) && $data->map != '') ? $data->map->Y : old('map[Y]')), array('class' => 'col-sm-5 col-sm-offset-1', 'placeholder' => 'Y', 'id' => 'mapY')) }}
                    </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3 text-center">
                    <div class="row">
                        <button type="button" data-toggle="modal" data-target="#add-map-modal" class="btn btn-sm btn-primary showTip L5">Открыть карту</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#ru" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#newsPhotos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">
             <div class="tab-pane active" id="ru">

                <div class="tabbable  tabs-left">

                 <ul id="myTab" class="nav nav-tabs">
                   <li class="active">
                      <a href="#descRu" data-toggle="tab">Описание</a>
                   </li>
                   <li class="lang-ro">
                      <a href="#descRo" data-toggle="tab">Описание на русском</a>
                   </li>
                   <li class="lang-en">
                      <a href="#descEn" data-toggle="tab">Описание на английском</a>
                   </li>
                 </ul>

                 <div class="tab-content">
                   <div class="tab-pane in active" id="descRu">
                     {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor')) }}
                   </div>
                   <div class="tab-pane" id="descRo" class="lang-ro">
                     {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
                   </div>
                   <div class="tab-pane" id="descEn" class="lang-en">
                     {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
                   </div>

                 </div>

                </div>
             </div>
            @include('admin.partials.meta')
            @include('admin.partials.photos', ['table' => 'tire_services', 'div_id' => 'newsPhotos', 'table_id' => isset($data->id) ? $data->id : 0])
        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

{{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug',['input_name'=>'name[ru]'])

    @include('admin.partials.datepicker')
    
    @include('admin.common.modals')
    
    <div aria-hidden="true" aria-labelledby="mySmallModalLabel" role="dialog"   data-backdrop="true" data-keyboard="true" class="modal fade" id="add-map-modal" >

        <div class="modal-dialog modal-md">
         <div class="close-modal" data-dismiss="modal"><i class="fa fa-close"></i></div>
            <div class="modal-content">
                <div class="modal-body">
                    <div id="YMapsID" style="width:100%;height:500px;"></div>
                </div>
            </div>
        </div>
    </div><!-- /popup map insert-->
    
    <script type="text/javascript">
         function AddMap(){
             var modal = $('#add-map-modal');
             modal.modal();
         }
    </script>

    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"> </script>
    
    <script type="text/javascript">
        @if(isset($data->map->X) && $data->map->X != 0)
            var x = '{{ $data->map->X }}';
        @else
            var x = '47.02615918';
        @endif

        @if(isset($data->map->Y) && $data->map->Y != 0)
            var y = '{{ $data->map->Y }}';
        @else
            var y = '28.83406047';
        @endif

        ymaps.ready(function () {
            var myMap = new ymaps.Map('YMapsID', {
                center: [x, y],
                zoom: 14,
                type: 'yandex#publicMap',
                behaviors: ['default', 'scrollZoom']
            });

            var searchControl = new ymaps.control.SearchControl({ noPlacemark: true });

            myMap.controls
                    .add('mapTools')
                    .add('zoomControl')
                    .add('typeSelector', { top: 5, right: 5 })
                    .add(searchControl, { top: 5, left: 200 });

            // Создаем геообъект с типом геометрии "Точка".
            myPlacemark = new ymaps.Placemark([x, y]);
            myPlacemark.options.set('draggable', true);
            myPlacemark.events.add('dragend', function (e) {
                var coords = myPlacemark.geometry.getCoordinates().toString().split(",");
                $('input#mapX').val(coords[0]);
                $('input#mapY').val(coords[1]);
            });

            myMap.geoObjects.add(myPlacemark);

            searchControl.events.add("resultselect", function (e) {
                var coordsObj = searchControl.getResultsArray()[0].geometry.getCoordinates();
                var coords = coordsObj.toString().split(",");
                $('input#mapX').val(coords[0]);
                $('input#mapY').val(coords[1]);
                myPlacemark.getOverlay().getData().geometry.setCoordinates(coordsObj);
            });

        });

        function changeCoords(coords){

        }

        function DoneClick(){
            var str = $("#markerPosition").val();
            var arr = str.split(',');
            var x_coord = arr[0].trim();
            var y_coord = arr[1].trim();
            $("input[name=mapX]").val(x_coord);
            $("input[name=mapY]").val(y_coord);
            tb_remove();
        }

    </script>

@endsection


