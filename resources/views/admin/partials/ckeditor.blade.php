{!! HTML::script('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') !!}
<script>
    $(document).ready(function(){
        var FORM_ID = '{{ $form_id or '' }}';
        if (FORM_ID != "") FORM_ID = "#" + FORM_ID + " ";
        $(FORM_ID + '.ckeditor').each(function(){

            CKEDITOR.config.language = 'ru';

            CKEDITOR.plugins.addExternal( 'justify', '/js/plugins/ckeditor_plugins/justify/', 'plugin.js' );

            CKEDITOR.replace( $(this).attr('id'), {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
                extraPlugins: 'justify'
            });

        });
    });
</script>