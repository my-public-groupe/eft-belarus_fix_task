<tr class="">
    <td align="right">
        {{ $counter }}
    </td>
    <td>@if(isset($model)){{ $model->manufacturer->name }}@endif</td>
    <td>
        @if(isset($model))<a href="{{ URL::to('admin/models/' . $model->id . '/edit') }}">{{ $model->name }}</a>@endif
    </td>
    <td>
        @if(isset($product))
        <a href="{{ URL::to('admin/products/' . $product->id . '/edit') }}"
           class="modalbox"
           title="Редактирование комплекта">
            {{ $product->product_number }}
        </a>
        @endif
    </td>
</tr>