@extends('admin.body')
@section('title', 'Модель')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/products') }}?category_id={{ (isset($data) ? $data->category_id : app('request')->input('category_id')) }}">Товары</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование модели</small> </h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/models', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/models/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    {{ Form::hidden('category_id', (isset($data) ? $data->category_id : app('request')->input('category_id'))) }}

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o bigger-120"></i> Сохранить </button>
            </div>
            @if (isset($data))
            <div class="col-sm-2">
                    <a href="javascript:void(0);" class="btn btn-danger btn-block btn-responsive" id="deletemodel">Удалить</a>
            </div>
            @endif
            <div class="col-sm-2">
                <div>
                    <label>
                        @if (isset($data))
                            {{ Form::checkbox('enabled',  1, ($data->enabled == 1 ? true : false), ['class' => 'ace']) }}
                        @else
                            {{ Form::checkbox('enabled',  1, true, ['class' => 'ace']) }}
                        @endif
                        <span class="lbl"> Включено </span>
                    </label>
                </div>
                <div>
                    <label>
                        @if (isset($data))
                            {{ Form::checkbox('top',  1, ($data->top == 1 ? true : false), ['class' => 'ace']) }}
                        @else
                            {{ Form::checkbox('top',  1, true, ['class' => 'ace']) }}
                        @endif
                        <span class="lbl"> На главной </span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">
                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Название', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group lang-ro">
                {{ Form::label('name[ro]', 'Наименование рум', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group lang-en">
                {{ Form::label('name[en]', 'Наименование англ', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[en]', (isset($data->name_en) ? $data->name_en : old('name_en')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('price', ' Цена', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('price', (isset($data->price) ? $data->price : old('price')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('product_text', 'Надпись', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('product_text', (isset($data->product_text) ? $data->product_text : old('product_text')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" id="mydate" class="form-control date-picker"
                               data-date-format="dd-mm-yyyy"
                               value="{{ (isset($data->created_at) ? date('d-m-Y', strtotime($data->created_at)) : old('date', Date::now()->format('d-m-Y'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('slug', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-12 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('manufacturer_id', 'Производитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('manufacturer_id', $manufacturers, (isset($data) ? $data->manufacturer_id : 0), ['class' => 'form-control chosencat']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('sort', 'Порядок сортировки', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('sort', (isset($data->sort) ? $data->sort : old('sort')), array('class' => 'col-sm-12 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#description" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#features" data-toggle="tab">Характеристики</a>
            </li>
            <li>
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
            <li>
                <a href="#products" data-toggle="tab">Комплекты</a>
            </li>
            <li>
                <a href="#meta" data-toggle="tab">META</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="description">
                <div class="tabbable  tabs-left">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#descRu" data-toggle="tab">Описание</a>
                        </li>
                        <li class="lang-ro">
                            <a href="#descRo" data-toggle="tab">Описание на румынском</a>
                        </li>
                        <li class="lang-en">
                            <a href="#descEn" data-toggle="tab">Описание на английском</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane in active" id="descRu">
                            {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor')) }}
                        </div>
                        <div class="tab-pane lang-ro" id="descRo">
                            {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
                        </div>
                        <div class="tab-pane lang-en" id="descEn">
                            {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="features">
                <div class="tabbable tabs-left">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#descRu" data-toggle="tab">Описание</a>
                        </li>
                        <li class="lang-ro">
                            <a href="#descRo" data-toggle="tab">Описание на румынском</a>
                        </li>
                        <li class="lang-en">
                            <a href="#descEn" data-toggle="tab">Описание на английском</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane in active" id="descRu">
                            {{ Form::textarea('description_short[ru]', (isset($data->description_short) ? $data->description_short : old('description_short')), array('class' => 'ckeditor', 'id' => 'editor_features')) }}
                        </div>
                        <div class="tab-pane lang-ro" id="descRo">
                            {{ Form::textarea('description_short[ro]', (isset($data->description_short_ro) ? $data->description_short_ro : old('description_short_ro')), array('class' => 'ckeditor', 'id' => 'editor_features_ro')) }}
                        </div>
                        <div class="tab-pane lang-en" id="descEn">
                            {{ Form::textarea('description_short[en]', (isset($data->description_short_en) ? $data->description_short_en : old('description_short_en')), array('class' => 'ckeditor', 'id' => 'editor_features_en')) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="products">
                <div class="row">
                    <div class="col-xs-12">
                        <a href="{{ url('admin/products/create') }}?{{ http_build_query(['category_id' => (isset($data) ? $data->category_id : app('request')->input('category_id')),
                            'model_id' => (isset($data->id) ? $data->id : null)]) }}"
                           class="btn btn-yellow modalbox" title="Добавить комплект">Добавить комплект</a>
                    </div>
                </div>
                <div class="space"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <table id="simple-table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="detail-col">ID</th>
                                <th>Наименование</th>
                                <th class="hidden-480">Доп. информация</th>
                                <th class="center">Сортировка</th>
                                <th>
                                    <i class="ace-icon fa fa-eye-slash bigger-130"></i>
                                </th>
                                <th>
                                    <i class="menu-icon fa fa-cogs"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($kits) && $kits->isNotEmpty())
                                @foreach($kits as $d)
                                    @include('admin.partials.table_tr', ['table'    => 'products',
                                                                         'id'       => $d->id,
                                                                         'name'     => $d->product_number,
                                                                         'enabled'  => $d->enabled,
                                                                         'sort'     => $d->sort
                                                                         ] )
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div><!-- /.span -->
                </div>
            </div>

            @include('admin.partials.meta')

            @include('admin.partials.photos', ['table' => 'models', 'table_id' => isset($data->id) ? $data->id : 0])

        </div>
    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}
    </div>

    {{ Form::close() }}
@endsection

@section('scripts')

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug',['input_name'=>'name[ru]'])

    @include('admin.partials.datepicker')

    @include('admin.common.modals')

    @include('admin.partials.visibility')

    @include('admin.partials.ajaxdelete')
    
    @include('admin.partials.chosen')

    <script>
        var modalSuccessSubmit = function(result) {
            var stop     = false;
            var table    = result.table;
            var block_id = "";
            switch (table) {
                case 'products':
                    block_id = "products";
                    break;
            }
            var $TABLE      = $("#" + block_id + ' table tbody');
            //смотрим если уже был такой id, т.е. update
            $TABLE.find('tr').each(function(){
                var cur_id = $(this).find("input").val();
                if (cur_id == result.id){
                    $(this).find('a.modalbox:first').text(result.name);
                    stop = true;
                }
            });
            if (stop) return true;
            var $TR         = $('<tr></tr>');
            var $TD         = $('<td class="center"></td>');
            var $INPUT      = $('<td class="center"><input type="hidden" name="'+table+'[]" value="' + result.id + '"  /><span>'+result.id+'</span></td>');
            var $NAME       = $('<td><a href="admin/'+ table +'/' + result.id + '/edit" class="modalbox" title="Редактирование: ' + result.name + '"> ' + result.name + '</a></td>');
            $TABLE.append(
                    $TR.append($INPUT)
                            .append($NAME)
                            .append($TD)
                            .append($TD.clone())
                            .append($TD.clone())
                            .append($TD.clone())
            );
            initModals();
        };

        $("a#deletemodel").click(function () {
            $.post("/admin/models/@if (isset($data->id)){{$data->id}}@endif", {
                "_method": "DELETE",
                "_token": "{{ csrf_token() }}"
            }).done(function () {
                window.location.replace("/admin/products?category_id=@if (isset($data->category_id)){{$data->category_id}}@endif");
            });
        });
    </script>
@endsection