
<ul class="nav nav-list">
    <li class="">
        <a href="{{ url('admin') }}">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Главная </span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/categories') }}">
            <i class="menu-icon fa fa-book"></i>
            <span class="menu-text"> Категории </span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/manufacturers') }}">
            <i class="menu-icon fa fa-tag"></i>
            <span class="menu-text"> Производители </span>
        </a>
    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-tablet"></i>
            <span class="menu-text"> Товары </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            @foreach(\App\Models\Categories::orderBy('name')->get() as $category)
            <li class="">
                <a href="admin/products?category_id={{$category->id}}">
                    {{$category->name}}
                </a>
            </li>
            @endforeach
        </ul>
    </li>
    <li>
        <a href="{{ url('admin/news') }}">
            <i class="menu-icon fa fa-newspaper-o"></i>
            <span class="menu-text"> Новости </span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/content') }}">
            <i class="menu-icon fa fa-file-text-o"></i>
            <span class="menu-text"> Страницы </span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/lists?id=2') }}">
            <i class="menu-icon fa fa-image"></i>
            <span class="menu-text"> Слайдер </span>
        </a>
    </li>
    <li class="">
        <a href="{{ url('admin/lists/1/edit') }}">
            <i class="menu-icon fa fa-building-o"></i>
            <span class="menu-text"> Контакты </span>
        </a>
    </li>
    {{--<li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-file-text-o"></i>
            <span class="menu-text"> Блоки </span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href="admin/lists?id=1">На Главной</a>
            </li>
            <li class="">
                <a href="admin/lists?id=4">Мы предлагаем</a>
            </li>
            <li class="">
                <a href="admin/lists/8/edit">Стоимость аренды оборудования</a>
            </li>
            <li class="">
                <a href="admin/lists?id=9">Футер</a>
            </li>
        </ul>
    </li>--}}
    {{--<li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-cogs"></i>
            <span class="menu-text"> Администр.</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href="{{ url('admin/users') }}">
                    <i class="menu-icon fa fa-users"></i>
                    Пользователи
                </a>
                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="{{ url('admin/parameters') }}">
                    <i class="menu-icon fa fa-tasks"></i>
                    <span class="menu-text"> Параметры </span>
                </a>
            </li>
            <li class="hide">
                <a href="{{ url('admin/translations') }}">
                    <i class="menu-icon fa fa-globe"></i>
                    <span class="menu-text"> Переводы </span>
                </a>
            </li>
            <li class="hide">
                <a href="{{ url('admin/lists') }}">
                    <i class="menu-icon fa fa-list-ol"></i>
                    <span class="menu-text"> Справочники </span>
                </a>
            </li>
            <li class="">
                <a href="{{ url('admin/clear-cache') }}">
                    <i class="menu-icon fa fa-trash-o"></i>
                    <span class="menu-text"> Очистить кеш </span>
                </a>
            </li>
        </ul>
    </li>--}}
</ul><!-- /.nav-list -->
