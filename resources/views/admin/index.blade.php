@extends('admin.body')
@section('title', 'Главная')

@section('centerbox')
<div class="page-header">
    <h1> Главная </h1>
</div>

<div class="alert alert-success">
    <button class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
    Здравствуйте, {{ Auth::user()->name }}! Управление сайтом в панели слева.
</div>

<div class="row main-page">
    <div class="col-xs-12">
        <h3 class="header smaller lighter green"></h3>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/categories') }}" class="btn btn-primary btn-app radius-4">
                    <i class="ace-icon fa fa-book bigger-230"></i>
                    Категории
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/manufacturers') }}" class="btn btn-purple btn-app radius-4">
                    <i class="ace-icon fa fa-tag bigger-230"></i>
                    Производители
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/lists?id=2') }}" class="btn btn-warning btn-app radius-4">
                    <i class="ace-icon fa fa-image bigger-230"></i>
                    Слайдер
                </a>
            </div>
            {{--<div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/news') }}" class="btn btn-success btn-app radius-4">
                    <i class="ace-icon fa fa-newspaper-o bigger-230"></i>
                    Новости
                </a>
            </div>--}}
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/lists/1/edit') }}" class="btn btn-success btn-app radius-4">
                    <i class="ace-icon fa fa-building-o bigger-230"></i>
                    Контакты
                </a>
            </div>
        </div><!-- /.row -->
        <div class="space"></div>
    </div><!-- /.col-xs-12 -->
</div>
@stop
