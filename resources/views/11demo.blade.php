@extends('body')
@section('title', 'Demo-показ')
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">Демо-показ</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header no-margin text-center">Демо-показ</h2>
            <br>
            <img src="images/demo.jpg" class="img-responsive center-block">
            <br>
        </div>
    </div>
    <div class="test-drive-container">
        @if(!empty($demo_page))
            {!! $demo_page->description !!}
        @endif

        <form role="form" method="post" action="{{ route('send-demo') }}" class="demo-frm-page">
            <div class="form-group">
                <label for="product">Оборудование*</label>
                <select class="form-control" name="d_product" id="product">
                    <option value="">--- Выберите оборудование ---</option>
                    @foreach($products as $product)
                        <option value="{{$product->name}}">{{$product->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="testdrive_fio">ФИО*</label>
                <input type="text" class="form-control" name="d_fio" id="fio" placeholder="ФИО">
            </div>
            <div class="form-group">
                <label for="testdrive_company">Название компании*</label>
                <input type="text" class="form-control" name="d_company" id="company" placeholder="Название компании">
            </div>
            <div class="form-group">
                <label for="testdrive_phone">Телефон*</label>
                <input type="text" class="form-control" name="d_phone" id="phone" placeholder="Телефон">
            </div>
            <div class="form-group">
                <label for="testdrive_message">Комментарий</label>
                <textarea class="form-control" name="d_comment" id="comment" placeholder="Комментарий"></textarea>
            </div>
            <button type="submit" class="btn btn-theme btn-testdrive">Заказать демо-показ!</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $(".demo-frm-page").validate({
                rules:{
                    d_product: {required: true},
                    d_fio: {required: true},
                    d_company: {required: true},
                    d_phone: {required: true},
                    d_email: {
                        required: true,
                        email: true
                    }
                },
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function (form) {
                    $.ajax({
                        method: "POST",
                        headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                        url: $(form).attr("action"),
                        data: $(form).serialize(),
                        success: function () {
                            swal({
                                type: 'success',
                                title: 'Спасибо! Ваша заявка отправлена',
                                showConfirmButton: false,
                                timer: 5000
                            });

                            $(form).trigger('reset');
                        }
                    });

                    return false;
                }
            });
        });
    </script>
@endsection