<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<p>Новая заявка на аренду:</p>
<b>Оборудование:</b> {{ $model }}
<br>
<b>Комплект:</b> {{ $package }}
<br>
<br>
<b>ФИО:</b> {{ $fio }}
<br>
<b>Телефон:</b> {{ $phone }}
<br>
<b>Email:</b> {{ $email }}
<br>
<b>Город:</b> {{ $city }}
<br>
<b>Компания:</b> {{ $company }}
<br>
<b>Дата с:</b> {{ $date_from }}
<br>
<b>Дата по:</b> {{ $date_to }}
<br>
<b>Комментарий:</b> {{ $comment }}