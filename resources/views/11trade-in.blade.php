@extends('body')
@section('title', 'Trade-in')
@section('centerbox')
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">Trade-in</li>
    </ol>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header no-margin text-center">Trade-in</h2>
            <br>
            <img src="images/tradein.png" class="img-responsive center-block">
            <br>
        </div>
    </div>
    <div class="test-drive-container">
        @if(!empty($trade_in_page))
            {!! $trade_in_page->description !!}
        @endif

        <form role="form" method="post" action="{{ route('send-trade-in') }}" class="trade-in-frm">
            <div class="form-group">
                <label for="product">Оборудование*</label>
                <select class="form-control" name="product" id="product">
                    <option value="">--- Выберите оборудование ---</option>
                    @foreach($products as $product)
                        <option value="{{$product->name}}">{{$product->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="testdrive_fio">ФИО*</label>
                <input type="text" class="form-control" name="fio" id="fio" placeholder="ФИО">
            </div>
            <div class="form-group">
                <label for="testdrive_company">Название компании*</label>
                <input type="text" class="form-control" name="company" id="company" placeholder="Название компании">
            </div>
            <div class="form-group">
                <label for="testdrive_phone">Телефон*</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Телефон">
            </div>
            <div class="form-group">
                <label for="testdrive_message">Комментарий</label>
                <textarea class="form-control" name="comment" id="comment" placeholder="Комментарий"></textarea>
            </div>
            <button type="submit" class="btn btn-theme btn-testdrive">Отправить</button>
        </form>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $(".trade-in-frm").validate({
                rules:{
                    product: {required: true},
                    fio: {required: true},
                    company: {required: true},
                    phone: {required: true},
                    email: {
                        required: true,
                        email: true
                    }
                },
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function (form) {
                    $.ajax({
                        method: "POST",
                        headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
                        url: $(form).attr("action"),
                        data: $(form).serialize(),
                        success: function () {
                            swal({
                                type: 'success',
                                title: 'Спасибо! Ваша заявка отправлена',
                                showConfirmButton: false,
                                timer: 5000
                            });

                            $(form).trigger('reset');
                        }
                    });

                    return false;
                }
            });
        });
    </script>
@endsection