@extends('body')
@include('partials.meta', ['data' => $data, 'title' => $data->name])
@section('centerbox')
    <!-- BREADCRUMBS -->
    <ol class="breadcrumb">
        <li><a href="{{route('index')}}">Главная</a></li>
        <li class="active">{{$data->name}}</li>
    </ol>
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="page-header no-margin">{{$data->name}}</h2>
            </div>
        </div>
    <script type="application/ld+json">
            {
             "@context": "http://schema.org",
             "@type": "BreadcrumbList",
             "itemListElement":
             [
              {
               "@type": "ListItem",
               "position": 1,
               "item":
               {
                "@id": "{{ route('index') }}",
                "name": "Главная"
                }
              },
              {
               "@type": "ListItem",
               "position": 2,
               "item":
               {
                "name": "{{$data->name}}"
                }
              }
             ]
            }

    </script>

    <!-- /BREADCRUMBS -->

    <!-- PAGE -->
    <div class="row">
        <div class="col-sm-12 no-margin">
            {!! $data->description !!}
        </div>
    </div>
    </div>
    <!-- /PAGE -->
@endsection