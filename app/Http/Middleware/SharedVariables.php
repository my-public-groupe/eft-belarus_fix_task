<?php

namespace App\Http\Middleware;

use App\Models\Categories;
use App\Models\Content;
use App\Models\Lists;
use Closure;

class SharedVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = Categories::enabled()->get();

        $content = Content::get();
        $tradein = Content::where('id', 8)->first();
        $contacts_list = Lists::find(1);

        view()->share('categories', $categories);
        view()->share('content', $content);
        view()->share('tradein', $tradein);
        view()->share('contacts_list', $contacts_list);

        return $next($request);
    }
}
