<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Manufacturers;
use App\Models\MenuProducts;
use App\Models\Models;
use App\Models\Products;
use App\Models\Subscribe;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Response;

use Newsletter;

class MailController extends Controller
{
    private $from_mail;
    private $from_name;
    private $to_mail;
    private $to_name;
    private $regex;

    public function __construct()
    {
        /*$this->from_mail = config('mail.from.address');
        $this->from_name = config('mail.from.name');
        $this->to_mail   = config('mail.to.address');
        $this->to_name   = config('mail.to.name');*/

        $this->from_mail = 'noreply@eft.by';
        $this->from_name = 'EFTplus Беларусь';
        $this->to_mail = ['info@eft.by', 'avb@eftgroup.ru'];
        $this->to_name = '';

        $this->regex = '/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/i';
    }

    public function newsletterSubscribe(Request $request)
    {

        //антибот
        if ($request->message == "") {
            Newsletter::subscribe($request->email);
        }

        return Response::json(["success" => true]);
    }

    public function sendTestdrive(Request $request)
    {
        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.send-testdrive',
                    [
                            'product' => $request->product,
                            'fio' => $request->fio,
                            'company' => $request->company,
                            'phone' => $request->phone,
                            'comment' => $request->comment
                    ], function ($m) {
                        $m->from($this->from_mail, $this->from_name)
                                ->to($this->to_mail, $this->to_name)
                                ->subject('[EFT.BY] Заявка на тест-драйв');
                    });
        }

        return Response::json(["success" => true]);
    }

    public function sendDemo(Request $request)
    {
        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.send-demo',
                    [
                            'product' => $request->d_product,
                            'fio' => $request->d_fio,
                            'company' => $request->d_company,
                            'phone' => $request->d_phone,
                            'comment' => $request->d_comment
                    ], function ($m) {
                        $m->from($this->from_mail, $this->from_name)
                                ->to($this->to_mail, $this->to_name)
                                ->subject('[EFT.BY] Заявка на демо-показ');
                    });
        }

        return Response::json(["success" => true]);
    }

    public function sendTradeIn(Request $request)
    {
        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.send-tradein',
                    [
                            'product' => $request->product,
                            'fio' => $request->fio,
                            'company' => $request->company,
                            'phone' => $request->phone,
                            'comment' => $request->comment
                    ], function ($m) {
                        $m->from($this->from_mail, $this->from_name)
                                ->to($this->to_mail, $this->to_name)
                                ->subject('[EFT.BY] Заявка на  Trade-in');
                    });
        }

        return Response::json(["success" => true]);
    }

    public function sendConsultation(Request $request)
    {
        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.send-consultation',
                [
                    'product' => $request->d_product,
                    'fio' => $request->d_fio,
                    'company' => $request->d_company,
                    'phone' => $request->d_phone,
                    'comment' => $request->d_comment
                ], function ($m) {
                    $m->from($this->from_mail, $this->from_name)
                        ->to($this->to_mail, $this->to_name)
                        ->subject('[EFT.BY] Заявка на Консультацию');
                });
        }

        return Response::json(["success" => true]);
    }

    public function sendContactForm(Request $request)
    {
        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.send-contactform',
                    [
                            'fio' => $request->fio,
                            'email' => $request->email,
                            'msg' => $request->msg
                    ], function ($m) {
                        $m->from($this->from_mail, $this->from_name)
                                ->to($this->to_mail, $this->to_name)
                                ->subject('[EFT.BY] Сообщение с сайта');
                    });
        }

        return Response::json(["success" => true]);
    }

    public function sendOrderCalc(Request $request)
    {

        $category = (!empty($request->calc_package) ? Categories::find($request->calc_type)->name : '');
        $manufacturer = (!empty($request->calc_package) ? Manufacturers::find($request->calc_manufacturer)->name : '');
        $model = (!empty($request->calc_package) ? Models::find($request->calc_model)->name : '');
        $package = (!empty($request->calc_package) ? Products::find($request->calc_package)->product_number : '');

        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.sendordercalc',
                    [
                            'calc_type' => $category,
                            'calc_manufacturer' => $manufacturer,
                            'calc_model' => $model,
                            'calc_package' => $package,
                            'quantity' => $request->quantity,
                            'date_from' => $request->date_from,
                            'date_to' => $request->date_to,
                            'fio' => $request->fio,
                            'mobile' => $request->mobile,
                            'email' => $request->email
                    ],
                    function ($m) {
                        $m->to($this->to_mail, $this->to_name)
                                ->subject('Заявка на аренду');
                    });
        }
    }

    public function sendOrderModel(Request $request)
    {
        $model = Models::where('slug', $request->model_slug)->first();
        $package = (!empty($request->package_id) ? Products::find($request->package_id)->product_number : '');

        $g_recaptcha = $request->get('g-recaptcha-response');

        if ($this->checkRecaptcha($g_recaptcha)) {
            Mail::send('emails.sendordermodel',
                    [
                            'model' => $model->name,
                            'package' => $package,
                            'fio' => $request->fio,
                            'phone' => $request->phone,
                            'email' => $request->email,
                            'city' => $request->city,
                            'company' => $request->company,
                            'date_from' => $request->date_from,
                            'date_to' => $request->date_to,
                            'comment' => $request->comment
                    ],
                    function ($m) {
                        $m->to($this->to_mail, $this->to_name)
                                ->subject('[EFT.BY] Заявка на аренду');
                    });
        }
    }

    public function sendCard(Request $request)
    {
        $data = [];
        foreach ($request->data as $id => $quantity) {
            $product = MenuProducts::find($id);
            $data[] = [
                    'name' => $product->parent->name . " / " . $product->name,
                    'quantity' => $quantity,
                    'price' => $product->price,
                    'amount' => $quantity * $product->price
            ];
        }

        $params = [
                'name' => $request->cartName,
                'email' => $request->cartEmail,
                'phone' => $request->cartPhone,
                'address' => $request->cartAdress,
                'text' => $request->cartMessage,
                'data' => $data
        ];

        Mail::send('emails.send-card', $params, function ($m) {
            $m->from($this->from_mail, $this->from_name)
                    ->to($this->to_mail, $this->to_name)
                    ->subject('Корзина с сайта');
        });
    }

    public function makeEvent(Request $request)
    {
        Mail::send('emails.make-event',
                ['name' => $request->name, 'phone' => $request->phone, 'text' => $request->text], function ($m) {
                    $m->from($this->from_mail, $this->from_name)
                            ->to($this->to_mail, $this->to_name)
                            ->subject('Организация эвента');
                });
    }

    public function makeContact(Request $request)
    {
        Mail::send('emails.make-contact',
                ['name' => $request->name, 'email' => $request->email, 'text' => $request->text], function ($m) {
                    $m->from($this->from_mail, $this->from_name)
                            ->to($this->to_mail, $this->to_name)
                            ->subject('Сообщение обратной связи');
                });
    }

    public function putEvent(Request $request)
    {
        Mail::send('emails.make-event',
                ['name' => $request->name, 'phone' => $request->phone, 'text' => $request->text], function ($m) {
                    $m->from($this->from_mail, $this->from_name)
                            ->to($this->to_mail, $this->to_name)
                            ->subject('Сообщения об эвенте');
                });
    }

    public function sendEmail(Request $request)
    {
        Mail::send('emails.send-email', ['clientEmail' => $request->clientEmail], function ($m) {
            $m->from($this->from_mail, $this->from_name)
                    ->to($this->to_mail, $this->to_name)
                    ->subject('Подписка на рассылку');
        });
    }

    public function reservation(Request $request)
    {
        Mail::send('emails.make-event',
                ['name' => $request->name, 'phone' => $request->phone, 'text' => $request->text], function ($m) {
                    $m->from($this->from_mail, $this->from_name)
                            ->to($this->to_mail, $this->to_name)
                            ->subject('Резервация заведения');
                });
    }

    function checkRecaptcha($token)
    {
        if (app()->environment() == 'testing') {
            return true;
        }

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => config('services.recaptcha.secret'),
            'response' => $token,
            'remoteip' => $remoteip
        ];
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);
        if ($resultJson->success != true) {
            return false;
        }
        if ($resultJson->score >= 0.3) {
            return true;
        } else {
            return false;
        }

    }
}
