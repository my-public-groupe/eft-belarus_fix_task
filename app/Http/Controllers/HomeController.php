<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Categories;
use App\Models\Lists;
use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Models\Content;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Models::with('photos', 'categories')
                ->where('top', 1)
                ->get();

        /*$block_main = Lists::where('id', 2)->first();
        $block_main_btn = Lists::where('id', 3)->first();

        $block_we_offer = Lists::where('id', 4)->with('children', 'children.photos')->first();

        $block_rent_cost = Lists::where('id', 8)->with('photos')->first();*/

        $slider = Lists::where('id', 2)->with('children', 'children.photos')->first();

        return view('index')->with(compact('products', 'slider'));
    }

    public function getManufacturers(Request $request){
        $category_id = $request->category_id;

        $out = array();

        $manufacturers = Manufacturers::whereHas('models', function ($query) use ($category_id){
                $query->where('category_id', $category_id);
        })->get();

        foreach ($manufacturers as $m){
            $out[] = [
                "id" => $m->id,
                "name" => $m->name
            ];
        }

        return Response::json($out);
    }

    public function getModels(Request $request){
        $manufacturer_id = $request->manufacturer_id;
        $category_id = $request->category_id;

        $models = array();

        $query = Models::where("manufacturer_id", $manufacturer_id)->where("category_id", $category_id)->orderBy('sort', 'asc')->get();

        foreach ($query as $q){
            $models[] = [
                "id" => $q->id,
                "name" => $q->name,
                "price" => $q->price
            ];
        }

        return Response::json($models);
    }

    public function getProducts(Request $request){
        $model_id = $request->model_id;

        $products = array();

        $query = Products::where("model_id", $model_id)->get();

        foreach ($query as $q){
            $products[] = [
                "id" => $q->id,
                "product_number" => $q->product_number,
                "price" => $q->price,
                "description" => $q->description
            ];
        }

        return Response::json($products);
    }

    public function getCatalog()
    {

        /*$categories = Categories::whereHas('models', function ($query) {
            $query->with('models');
        })->get();*/

        $categories = Categories::whereHas('models')->with('models')->orderBy('sort', 'asc')->get();

        return view('catalog.index')->with(compact('categories'));
    }

    public function getProduct()
    {
        return view('products.product');
    }

    public function getTerms()
    {
        return view('terms');
    }

    public function getAbout()
    {
        return view('about');
    }

    public function getContacts()
    {
        return view('contacts');
    }

    public function testDrive(){
        $testdrive_page = Content::find(9);

        $products = Models::enabled()->get();

        return view('test-drive')->with(compact('products', 'testdrive_page'));
    }

    public function demo(){
        $demo_page = Content::find(10);

        $products = Models::enabled()->get();

        return view('demo')->with(compact('products', 'demo_page'));
    }

    public function tradeIn(){
        $trade_in_page = Content::find(8);

        $products = Models::enabled()->get();

        return view('trade-in')->with(compact('products', 'trade_in_page'));
    }
}
