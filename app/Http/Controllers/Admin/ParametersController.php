<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\Parameters;
use App\Models\ParametersValues;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ParametersController extends Controller
{
    public function index()
    {
        $data = Parameters::all();
        return view('admin.parameters.index')->with(compact('data'));
    }

    public function create()
    {
        return view('admin.parameters.edit');
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'  => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Parameters();
        } else {
            $data = Parameters::find($id);
        }

        $data->name  = $request->name;
        $data->type  = $request->type;

        $data->save();

        if (isset($request->values)) {
            foreach ($request->values['id'] as $key => $value_id) {
                $ru = $request->values['ru'][$key];
                $ro = $request->values['ro'][$key];
                $en = $request->values['en'][$key];

                if ($ru == "" && $ro == "" && $en == "") continue;

                if ($value_id > 0) {
                    $parameter_value = ParametersValues::find($value_id);
                } else {
                    $parameter_value = new ParametersValues();
                }
                $parameter_value->value    = $ru;
                $parameter_value->value_ro = $ro;
                $parameter_value->value_en = $en;
                $parameter_value->sort     = $key;

                $data->values()->save($parameter_value);
            }
        }



        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/parameters');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Parameters::where('id', $id)->with('values')->first();

        return view('admin.parameters.edit')->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Parameters::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }
    
    public function removeProductParameter(Request $request)
    {
        $id            = $request->id;
        $parameters_id = $request->parameters_id;
        return response()->json(['success' => 'true']);
    }

    public function getCategoryParameters()
    {
        $category_id = Input::get('category_id', 0);
        $params = Categories::find($category_id)
                    ->parameters()
                    ->with(['parameter' => function($query){
                        $query->with('values');
                    }])
                    ->get();

        $data = [];
        foreach ($params as $param) {
            $data[] = [
                'id' => $param->parameter->id,
                'name' => $param->parameter->name,
                'type' => $param->parameter->type,
                'values' => $param->parameter->values->pluck('value', 'id')->toArray(),
            ];
        }

        return response()->json(['success' => 'true', 'data' => $data]);
    }
}
