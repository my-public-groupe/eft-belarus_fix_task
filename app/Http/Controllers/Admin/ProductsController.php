<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\ParametersProducts;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Models\Categories;
use Validator;

class ProductsController extends Controller
{
    private $rules;

    public function __construct()
    {
        $this->rules = [
            //'product_number' => 'required',
            //'price' => 'required',
            'category_id' => 'not_in:0'
        ];
    }

    public function index(Request $request)
    {
        $category_id   = $request->category_id;
        $category      = Categories::find($category_id);
        $manufacturers = Manufacturers::pluck('name', 'id')->toArray();

        //filters
        $filters['manufacturer'] = Input::get('manufacturer', 0);
        $filters['model']        = Input::get('model', '');

        $query = Models::where('category_id', $category_id);
                    if ($filters['manufacturer'] != 0) {
                        $query->where('manufacturer_id', $filters['manufacturer']);
                    }
                    if ($filters['model'] != '') {
                        $query->where('name', 'like', '%' . $filters['model'] . '%');
                    }

        $query->with('manufacturer');
        $query->with('products');
        $query->orderBy('sort', 'asc');

        $data = $query->paginate(50);

        $needs = [
            'manufacturers' => $manufacturers,
        ];

         return view('admin.products.index')
            ->with(compact('data', 'category', 'needs', 'filters'));
    }

    public function create()
    {
        $category_id = Input::get('category_id', 0);
        $parameters = Categories::find($category_id)
            ->parameters()
            ->with(['parameter' => function($query){
                $query->with('values');
            }])
            ->get();

        return view('admin.products.edit')
            ->with(compact('parameters'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data'    => $validator->messages()
            ]);
        }

        return $this->save($request, null);
    }
    
    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Products();
        } else {
            $data = Products::find($id);
        }

        $data->top               = ($request->top == 1) ? 1 : 0;
        $data->model_id          = $request->model_id;
        $data->product_number    = $request->product_number;
        $data->price             = $request->price;

        $data->description       = $request->description;

        $data->save();
        
        $this->UpdatePhotos($request, $data->id);

        //parameters
        if (isset($request->parameters_id)){
            $relations = [];
            foreach ($request->parameters_id as $key => $pid) {
                $ppid = $request->parameter_products_id[$key];
                if ($ppid == 0) {
                    $pp = new ParametersProducts();
                    $pp->parameters_id = $pid;
                } else {
                    $pp = ParametersProducts::find($ppid);
                }

                $pp->table = 'products';
                $pp->sort  = $key;

                if (isset($request->values['ru'][$pid])) {
                    $pp->value = $request->values['ru'][$pid];
                }

                if (isset($request->values['ro'][$pid])) {
                    $pp->value_ro = $request->values['ro'][$pid];
                }

                if (isset($request->values['en'][$pid])) {
                    $pp->value_en = $request->values['en'][$pid];
                }

                if (isset($request->values_id[$pid])) {
                    $pp->values_id = $request->values_id[$pid];
                }

                $pp->save();

                $relations[] = $pp;
            }

            $data->parameters()->saveMany($relations);
        }

        return response()->json([
            'success' => true,
            'data'    => ['table' => 'products', 'id' => $data->id, 'name' => $data->product_number]
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Products::where('id', $id)
            ->with('parameters')
            ->with('model')
            ->first();

        $category_parameters = Categories::find($data->model->category_id)
            ->parameters()
            ->with(['parameter' => function ($query) {
                $query->with('values');
            }])
            ->get();

        $product_parameters = $data->parameters;

        //собираем коллекцию из параметров категории,
        //а если есть параметр у продукта, заменяем на него
        $parameters = new Collection();
        foreach ($category_parameters as $cp) {
            if ($product_parameters->contains('parameters_id', $cp->parameters_id)) {
                $pp = $product_parameters->filter(function($item) use ($cp) {
                    return $item->parameters_id == $cp->parameters_id;
                })->first();
                $parameters->push($pp);
            } else {
                $parameters->push($cp);
            }
        }

       return view('admin.products.edit')->with(compact('data', 'parameters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'data'    => $validator->messages()
            ]);
        }

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $data = Products::find($id);
        $data->parameters()->delete();
        $data->photos()->delete();
        $data::destroy($id);
        if (!$request->ajax()){
            Session::flash('message', trans('common.deleted'));
            return back();
        } else {
            return response()->json(['success' => 'true']);
        }
    }
}
