<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lists;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ListsController extends Controller
{
    public function index(Request $request)
    {
        $id      = $request->id;

       if ($id > 0){
           $parent  = Lists::where('id', $id)->with('children')->first();
           $data    = $parent->children;
       }else{
           $data    = Lists::where('parent_id', 0)->get();
       }

       $parents = $this->getParentsArray();
       $model = 'lists';
       return view('admin.lists.index')->with(compact('data', 'parents', 'id', 'model', 'parent'));
    }

    public function create(Request $request)
    {
        //$parents    = $this->getParentsArray();
        $parent_id         = $request->id;
        $parents    = Lists::all()->pluck('name', 'id')->toArray();

        if($parent_id = 2){
            return view('admin.slider.edit')->with(compact('parents', 'parent_id'));
        }

        return view('admin.lists.edit')->with(compact('parents', 'parent_id'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request);
    }

    private function save(Request $request, $id = null)
    {
        // store
        if (!isset($id)) {
            $data = new Lists();
        }else{
            $data = Lists::find($id);
        }

        $data->name              = $request->name;
        $data->created_at        = $request->date;
        $data->slug              = $request->slug;
        $data->parent_id         = $request->parent_id;

        $data->description       = $request->description;
        $data->description_short = $request->description_short;
        /*$data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;*/


        $data->save();

        $this->UpdatePhotos($request, $data->id);

        // redirect
        Session::flash('message', trans('common.saved'));

        return redirect('admin/lists?id=' . $request->parent_id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data       = Lists::find($id);
        //$parents    = $this->getParentsArray();
        $parents    = Lists::all()->pluck('name', 'id')->toArray();
        $parent_id  = $data->parent_id;

        if($parent_id == 2){
            return view('admin.slider.edit')->with(compact('data', 'parents', 'parent_id'));
        }

        return view('admin.lists.edit')->with(compact('data', 'parents', 'parent_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Lists::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    private function getParentsArray()
    {
        $parent = Lists::where('parent_id', 0)->get();
        if ($parent->count() == 0){
            return [];
        }
        return $parent->pluck('name', 'id')->toArray();
    }
}
