<?php

namespace App\Http\Controllers\Admin;

use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\Photos;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ModelsController extends Controller
{
    public function create()
    {
        return view('admin.products.edit_model')->with($this->needs());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'category_id'   => 'not_in:0',
            'manufacturer_id'  => 'not_in:0',
            'slug'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    private function save(Request $request, $id)
    {
        // store
        if (!isset($id)) {
            $data = new Models();
            $data->category_id = $request->category_id;
        } else {
            $data = Models::find($id);
        }

        $data->name              = $request->name;
        $data->enabled           = ($request->enabled == 1) ? 1 : 0;
        $data->created_at        = $request->date;
        $data->slug              = $request->slug;
        $data->price             = $request->price;
        $data->description       = $request->description;
        $data->description_short = $request->description_short;
        $data->product_text      = $request->product_text;
        $data->meta_description  = $request->meta_description;
        $data->meta_keywords     = $request->meta_keywords;
        $data->title             = $request->title;
        $data->manufacturer_id   = $request->manufacturer_id;
        $data->sort              = $request->sort;
        $data->top               = $request->top;
        $data->save();

        $this->UpdatePhotos($request, $data->id);

        //products
        if ($request->products) {
            $sort_kits = $request->sort_kits;

            foreach ($request->products as $p){
                Products::where('id', $p)->update(
                    [
                        'model_id' => $data->id,
                        'sort' => $sort_kits[$p]
                    ]
                );
            }
        }

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/products?category_id=' . $request->category_id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Models::where('id', $id)->with('products.model')->first();

        $kits = $data->products->sortBy('sort');

        return view('admin.products.edit_model')->with(compact('data', 'kits'))->with($this->needs());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'          => 'required',
            'category_id'   => 'not_in:0',
            'manufacturer_id'  => 'not_in:0',
            'slug'          => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Products::where('model_id', $id)->with('photos')->delete();
        Photos::where('table', 'products')->where('table_id', $id)->delete();

        Models::destroy($id);
        Session::flash('message', trans('common.deleted'));

        return 1;
    }

    private function needs()
    {
        $manufacturers = Manufacturers::orderBy('name')->pluck('name', 'id');
        return compact('manufacturers');
    }
}
