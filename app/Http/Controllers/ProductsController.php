<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Content;
use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\Products;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\AssignOp\Mod;


class ProductsController extends Controller
{
    public function getTyresManufacturers()
    {
        $data = $this->getManufacturersData(1);
        $data['category'] = Categories::find(1);
        return view('catalog.index')->with($data);
    }

    public function getDisksManufacturers()
    {
        $data = $this->getManufacturersData(2);
        $data['category'] = Categories::find(2);
        return view('catalog.index')->with($data);
    }
    
    public function getManufacturer(Request $request)
    {
        $category = Categories::where('slug', $request->category_slug)->firstOrFail();
        $manufacturer = Manufacturers::where('slug', $request->manufacturer_slug)->firstOrFail();
        $models       = Models::enabled()
                        ->where('manufacturer_id', $manufacturer->id)
                        ->where('category_id', $category->id)
                        ->with('products.parameters')
                        ->with('products.model')
                        ->with('manufacturer')
                        ->paginate(12);

        $data = $this->getManufacturersData($category->id);
        $data['manufacturer'] = $manufacturer;
        $data['models'] = $models;
        $data['category'] = $category;
        return view('catalog.manufacturer')->with($data);
    }

    public function getCategory(Request $request){

        $category_slug = $request->category_slug;

        $category = Categories::where('slug', $category_slug)
                ->with('photos')
                ->firstOrFail();
                
        $description = $category->description;
        $pattern = '/<p>.*?<\/p>/s';
        preg_match_all($pattern, $description, $matches);
        $pTagsArray = $matches[0];

        $categoryDescription = '';
        $bannerImage = '';

        foreach ($pTagsArray as $pTag) {
            if (strpos($pTag, '<img') === false) {
                $categoryDescription .= $pTag;
            } else {
                $bannerImage .= $pTag;
            }
        }

        $category->description = $categoryDescription;
        $category->bannerImage = $bannerImage;

        $models = Models::enabled()
                  ->where('category_id', $category->id)
                  ->with('photos')
                  ->orderBy('sort')
                  ->get();

        return view('catalog.category')->with(compact('category','models'));
    }

    public function getModel(Request $request)
    {
        $category = Categories::where('slug', $request->category_slug)->firstOrFail();
        $model = Models::enabled()
                ->where('slug', $request->model_slug)
                ->with('manufacturer')
                ->with('photos')
                ->with('products.model')
                ->firstOrFail();

        //products types
        $products = [];
        foreach ($model->products as $item) {
            $products[$item->radius][] = $item;
        }
        //sort by keys
        ksort($products);

        $data['model'] = $model;
        $data['category'] = $category;
        $data['products'] = $products;
        $testdrive_page = Content::find(9);
        $demo_page = Content::find(10);
        return view('products.model')->with(compact('data', 'testdrive_page', 'demo_page'));
    }

    public function getProduct(Request $request)
    {
        $category = Categories::where('slug', $request->category_slug)->firstOrFail();
        $product  = Products::enabled()
                    ->with('photos')
                    ->with('model.manufacturer')
                    ->with('parameters.parameter.values')
                    ->where('id', $request->product_id)
                    ->firstOrFail();

        $data = $this->getManufacturersData($category->id);
        $data['category'] = $category;
        $data['product']  = $product;
        return view('products.product')->with($data);
    }

    private function getManufacturersData($category_id)
    {
        $ids = Models::select('manufacturer_id')->where('category_id', $category_id)->groupBy('manufacturer_id')->pluck('manufacturer_id')->toArray();

        $manufacturers = Manufacturers::enabled()
            ->whereIn('id', $ids)
            ->with('photos')
            ->get();

        $with_photos = new Collection();
        $without_photos = new Collection();
        foreach ($manufacturers as $manufacturer) {
            if ($manufacturer->photos->count() > 0) {
                $with_photos->push($manufacturer);
            } else {
                $without_photos->push($manufacturer);
            }
        }

        $data['manufacturers_with_photo']    = $with_photos;
        $data['manufacturers_without_photo'] = $without_photos;
        
        return $data;
    }
    
}
