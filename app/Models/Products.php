<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Products extends BaseModel
{
	protected $fillable = ['reserve'];

	public static function getBySlug($slug)
	{
	   	return self::where('slug', $slug)->enabled()->firstOrFail();
	}

	public function model()
	{
		return $this->hasOne('App\Models\Models', 'id', 'model_id');
	}

    public function getTypeSizeName()
	{
		if ($this->model->category_id == 1) {
			return $this->attributes['width']
			. '/' . $this->attributes['height']
			. '/' . 'R' . $this->attributes['radius'];
		} else {
			return $this->attributes['radius']
			. '/' . $this->attributes['width']
			. '/' . $this->attributes['height'];
		}
	}

	public function getSmallPhoto()
	{
		if ($this->attributes['img_100x200'] != '') {
			return $this->attributes['img_100x200'] . '.jpg';
		}

		return null;
	}

	public function getBigPhoto()
	{
		if ($this->attributes['imgs'] != '') {
			return $this->attributes['imgs'] . '.jpg';
		}

		return null;
	}

	public function getSmallPhotoPath()
	{
		$path = '/uploaded/';
		$DB_photo = $this->getSmallPhoto();
		if (isset($DB_photo)) {
			if ($this->model->category_id == 1) {
				$path .= 'tyres_images/';
			} elseif($this->model->category_id == 2) {
				$path .= 'disks_images/';
			}
			return $path . $DB_photo;
		}

		return $path . $this->mainphoto();
	}
	
	public function scopeNeeds($query)
	{
		return $query->with(['model' => function($q) {
			$q->with('manufacturer');
		}])->with('parameters')
			->with('photos');
	}

	public function getSeason()
	{
		if (!isset($this->parameters)) return 0;
		$arr = $this->parameters->pluck('values_id', 'parameters_id')->toArray();
		//Сезонность
		//parameters_id = 1
		if (isset($arr[1])) return $arr[1];
		return 0;
	}

	public function getFullName()
	{
		return $this->model->manufacturer->name
			. ' ' . $this->model->name
			. ' ' . $this->getTypeSizeName();
	}

	public static function getRadiusArray($category_id)
	{
		return Cache::get('radius' . $category_id, function () use ($category_id) {
			return self::getArray('radius', $category_id);
		});
	}

	public static function getWidthArray($category_id)
	{
		return Cache::get('width' . $category_id, function () use ($category_id) {
			return self::getArray('width', $category_id);
		});
	}

	public static function getHeightArray($category_id)
	{
		return Cache::get('height' . $category_id, function () use ($category_id) {
			return self::getArray('height', $category_id);
		});
	}

	private static function getArray($field, $category_id)
	{
		$data = Products::select($field)
			->groupBy($field)
			->orderBy($field)
			->whereHas('model', function($query) use ($category_id) {
				$query->where('category_id', $category_id);
			})
			->pluck($field, $field)
			->toArray();

		cache([$field . $category_id => $data], config('custom.arrays_cache_minutes'));
		return $data;
	}
}
