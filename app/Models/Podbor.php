<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Podbor extends Model
{
    protected $table = 'podbor_shini_i_diski';
    public $timestamps = false;

    public static function getAutoBrands()
    {
        return Cache::get('auto_brands', function () {
            $data = Podbor::select('vendor')
                        ->groupBy('vendor')
                        ->orderBy('vendor')
                        ->pluck('vendor')
                        ->toArray();

                        cache(['auto_brands' => $data], config('custom.arrays_cache_minutes'));
            return $data;
        });
    }

    public static function getHeightArray($category_id)
    {
        return Cache::get('height' . $category_id, function () use ($category_id) {
            return self::getArray('height', $category_id);
        });
    }

    private static function getArray($field, $category_id)
    {
        $data = Products::select($field)
            ->groupBy($field)
            ->orderBy($field)
            ->whereHas('model', function($query) use ($category_id) {
                $query->where('category_id', $category_id);
            })
            ->pluck($field, $field)
            ->toArray();

        cache([$field . $category_id => $data], config('custom.arrays_cache_minutes'));
        return $data;
    }
}
