<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParametersValues extends Model
{
    public $timestamps  =   false;
}
