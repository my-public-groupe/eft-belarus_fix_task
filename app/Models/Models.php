<?php

namespace App\Models;


class Models extends BaseModel
{
    protected $fillable = ['slug'];

    public function manufacturer()
    {
        return $this->hasOne('App\Models\Manufacturers', 'id', 'manufacturer_id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Products', 'model_id');
    }

    public function categories(){
        return $this->hasOne('App\Models\Categories', 'id', 'category_id');
    }
    
    public function getFullName()
    {
        return $this->manufacturer->name
        . ' ' . $this->name;
    }
}
