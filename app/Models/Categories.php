<?php

namespace App\Models;

class Categories extends BaseModel
{

    public function children() 
    {
	    return $this->belongsToMany('App\Models\Categories', 'categories_xref', 'parent_id', 'child_id');
    }
    
    public function parents() 
    {
	    return $this->belongsToMany('App\Models\Categories', 'categories_xref',  'child_id', 'parent_id');
    }

    public function models(){
        return $this->hasMany('App\Models\Models', 'category_id', 'id')->orderBy('sort', 'asc');
    }


}
