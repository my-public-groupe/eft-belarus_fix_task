<?php

namespace App\Models;


class Parameters extends BaseModel
{
    public $timestamps  =   false;

    public function values()
    {
        return $this->hasMany('App\Models\ParametersValues')->orderBy('sort');
    }
}
