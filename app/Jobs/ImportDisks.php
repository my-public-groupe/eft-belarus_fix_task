<?php

namespace App\Jobs;

use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\ParametersProducts;
use App\Models\Products;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class ImportDisks implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $task = 'import-disks';

        switch ($task) {
            case 'manufacturers':
                $this->importManufacturers();
                break;
            case 'import-disks':
                $this->importDisks();
                break;
            default:
                echo 'no-task';
        }
    }

    private function importDisks()
    {
        $dblist = DB::connection('mysql_import')
            ->table('table_225')
            ->where('reserve', '<>', 1)
            ->select()
            //->limit(100)
            ->get();

        $counter  = 0;
        foreach ($dblist as $item) {


            $manufacturer = Manufacturers::where('reserve', $item->vendor_id)->first();
            $model_slug = str_slug($item->name_short);
            if ($model_slug == '') continue;
            $model = Models::firstOrNew(['slug' => $model_slug, 'manufacturer_id' => $manufacturer->id, 'category_id' => 2]);
            $model->category_id  = 2; //диски
            $model->name = $item->name_short;
            $model->slug = $model_slug;
            $model->manufacturer_id = $manufacturer->id;
            $model->save();

            $product = Products::firstOrNew(['reserve' => $item->id, 'model_id' => $model->id]);
            $product->radius    = $this->getFloatValue($item->p_2);
            $product->width     = $this->getFloatValue($item->p_1);
            $product->height    = $this->getFloatValue($item->p_4);
            $product->reserve   = $item->id;
            $product->model_id  = $model->id;
            $product->img_100x200 = $item->img_100x200;
            $product->imgs      = $item->imgs;
            $product->save();

            //parameters
            //Вылет (ET)
            $value = trim($item->p_3);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 11;
                $pp->value = $value;
                $pp->save();
            }

            //Вес
            $value = trim($item->p_5);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 12;
                $pp->value = $value;
                $pp->save();
            }

            //Ширина обода
            $value = trim($item->p_6);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 13;
                $pp->value = $value;
                $pp->save();
            }

            //Диаметр обода
            $value = trim($item->p_7);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 14;
                $pp->value = $value;
                $pp->save();
            }

            //Тип
            $value = trim($item->p_8);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 15;
                $pp->values_id = $this->getType($value);
                $pp->save();
            }

            //Материал
            $value = trim($item->p_9);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 16;
                $pp->values_id = $this->getMaterial($value);
                $pp->save();
            }

            //Цвет
            $value = trim($item->p_10);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 17;
                $pp->value = $value;
                $pp->save();
            }

            DB::connection('mysql_import')
                ->table('table_225')
                ->where('id', $item->id)
                ->update(['reserve' => 1]);


            echo ++$counter . '.' . $item->name . PHP_EOL;
        }
    }

    private function importManufacturers()
    {
        $dblist = DB::connection('mysql_import')
            ->table('table_225')
            ->select(DB::raw('distinct(vendor_id), vendor as name'))
            ->orderBy('name')
            ->get();

        //dd($dblist);

        foreach ($dblist as $item) {
            if ($item->name == "") continue;
            $slug = str_slug($item->name);
            $manufacturer = Manufacturers::firstOrNew(['slug' => $slug]);
            $manufacturer->name = $item->name;
            $manufacturer->slug = $slug;
            $manufacturer->reserve = $item->vendor_id;
            $manufacturer->save();

            echo $item->name . PHP_EOL;
        }

        echo $dblist->count() . " items DONE";
    }

    private function getIntValue($value)
    {
        return (int) preg_replace('/[^0-9]/', '', $value);
    }

    private function getFloatValue($value)
    {
        return floatval($value);
    }

    private function getType($value)
    {
        if (str_contains($value, 'штампованные')) {
            return 14;
        }

        if (str_contains($value, 'литые')) {
            return 15;
        }

        if (str_contains($value, 'сборные')) {
            return 16;
        }

        if (str_contains($value, 'кованые')) {
            return 17;
        }

        return 0;
    }

    private function getMaterial($value)
    {
        if ($value == 'сталь') {
            return 18;
        }

        if (str_contains($value, 'алюминиевый сплав')) {
            return 19;
        }

        if (str_contains($value, 'магниевый сплав')) {
            return 20;
        }

        if (str_contains($value, 'сталь + алюминий')) {
            return 21;
        }

        if (str_contains($value, 'титановый сплав')) {
            return 22;
        }

        return 0;
    }
}
