<?php

namespace App\Jobs;

use App\Models\Manufacturers;
use App\Models\Models;
use App\Models\ParametersProducts;
use App\Models\Products;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class ImportTyres implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $task = 'import-tyres';

        switch ($task) {
            case 'manufacturers':
                $this->importManufacturers();
                break;
            case 'import-tyres':
                $this->importTyres();
                break;
            default:
                echo 'no-task';
        }
    }

    private function importTyres()
    {
        $dblist = DB::connection('mysql_import')
            ->table('table_168')
            ->where('reserve', '<>', 1)
            ->select()
            //->limit(100)
            ->get();

        $counter  = 0;
        foreach ($dblist as $item) {


            $manufacturer = Manufacturers::where('reserve', $item->vendor_id)->first();
            $model_slug = str_slug($item->name_short);
            if ($model_slug == '') continue;
            $model = Models::firstOrNew(['slug' => $model_slug, 'manufacturer_id' => $manufacturer->id, 'category_id' => 1]);
            $model->category_id  = 1; //шины
            $model->name = $item->name_short;
            $model->slug = $model_slug;
            $model->manufacturer_id = $manufacturer->id;
            $model->save();

            $product = Products::firstOrNew(['reserve' => $item->id, 'model_id' => $model->id]);
            $product->radius    = $this->getFloatValue($item->p_4);
            $product->width     = $this->getFloatValue($item->p_3);
            $product->height    = $this->getFloatValue($item->p_5);
            $product->reserve   = $item->id;
            $product->model_id  = $model->id;
            $product->img_100x200 = $item->img_100x200;
            $product->imgs      = $item->imgs;
            $product->vehicle_type = $this->getVehicleType($item->p_1);
            $product->save();

            //parameters
            //Сезонность
            $value = trim($item->p_2);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 1;
                $pp->values_id = $this->getSeason($value);
                $pp->save();
            }

            //Индекс скорости
            $value = trim($item->p_6);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 2;
                $pp->value = $value;
                $pp->save();
            }

            //Индекс нагрузки
            $value = trim($item->p_7);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 3;
                $pp->value = $value;
                $pp->save();
            }

            //Способ герметизации
            $value = $item->p_8;
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 4;
                $pp->values_id = $this->getHermetization($value);
                $pp->save();
            }

            //Конструкция
            $value = $item->p_9;
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 5;
                $pp->values_id = $this->getConstruction($value);
                $pp->save();
            }

            //Технология RunFlat
            $value = $item->p_10;
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 6;
                $pp->values_id = $this->getRunFlat($value);
                $pp->save();
            }

            //Шипы
            $value = $item->p_11;
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 7;
                $pp->values_id = $this->getShipi($value);
                $pp->save();
            }

            //Дополнительная информация
            $value = trim($item->p_12);
            if (isset($value) && $value != '') {
                $pp = new ParametersProducts();
                $pp->table = 'products';
                $pp->table_id = $product->id;
                $pp->parameters_id = 10;
                $pp->value = $value;
                $pp->save();
            }

            DB::connection('mysql_import')
                ->table('table_168')
                ->where('id', $item->id)
                ->update(['reserve' => 1]);


            echo ++$counter . '.' . $item->name . PHP_EOL;
        }
    }

    private function getConstruction($value)
    {
        if (str_contains($value, 'диагональные')) {
            return 6;
        }

        //радиальные
        return 7;
    }

    private function getRunFlat($value)
    {
        if (str_contains($value, 'нет')) {
            return 9;
        }

        if (str_contains($value, 'есть')) {
            return 8;
        }

        return 0;
    }

    private function getShipi($value)
    {
        if (str_contains($value, 'нет')) {
            return 12;
        }

        if (str_contains($value, 'есть')) {
            return 11;
        }

        return 0;
    }

    private function getHermetization($value)
    {
        if (str_contains($value, 'бескамерные')) {
            return 4;
        }

        //камерные
        return 5;
    }

    private function getSeason($value)
    {
        if (str_contains($value, 'летние')) {
            return 3;
        }

        if (str_contains($value, 'зимние')) {
            return 2;
        }

        if (str_contains($value, 'всесезонные')) {
            return 1;
        }
        return 0;
    }

    private function getVehicleType($dbtype)
    {
        if (str_contains($dbtype, 'внедорожник'))
            return 1;

        return 0;
    }

    private function importManufacturers()
    {
        $dblist = DB::connection('mysql_import')
            ->table('table_168')
            ->select(DB::raw('distinct(vendor_id), vendor as name'))
            ->orderBy('name')
            ->get();

        //dd($dblist);

        foreach ($dblist as $item) {
            if ($item->name == "") continue;
            $slug = str_slug($item->name);
            $manufacturer = Manufacturers::firstOrNew(['slug' => $slug]);
            $manufacturer->name = $item->name;
            $manufacturer->slug = $slug;
            $manufacturer->reserve = $item->vendor_id;
            $manufacturer->save();

            echo $item->name . PHP_EOL;
        }

        echo $dblist->count() . " items DONE";
    }

    private function getFloatValue($value)
    {
        return floatval($value);
    }


}
